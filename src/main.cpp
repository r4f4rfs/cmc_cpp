#include <cstdio>
#include <iostream>
#include <memory>

#include <lemon/random.h>
#include <lemon/time_measure.h>

#include "algorithms/cmc.h"
#include "algorithms/dummy.h"
#include "algorithms/dijkstra.h"
#include "algorithms/backtrack.h"
#include "algorithms/intersection.h"
#include "algorithms/mcmcmin.h"
#include "algorithms/ilp.h"
#include "simulator.h"

using namespace std;
using namespace cmc;
using namespace algorithms;

static const unsigned int MAX_PATHS = 5;
static const unsigned long long tlimit = 2 * 60;
static const bool disjoint = true;

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
	return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

static void
init_alglist(std::vector<std::unique_ptr<CMC>>& alglist, int npaths)
{
	if (npaths > 1) {
		alglist.push_back(make_unique<MCMCMin>(tlimit, disjoint));
		alglist.push_back(make_unique<MBacktrack>(tlimit, disjoint));
#if HAVE_CPLEX
		alglist.push_back(make_unique<LPMulti>(tlimit, disjoint));
#endif
		return;
	}
	alglist.push_back(make_unique<Backtrack>(tlimit / 2));
	alglist.push_back(make_unique<Backtrack>(tlimit));
	alglist.push_back(make_unique<DijkstraT>());
	alglist.push_back(make_unique<DijkstraX>());
	alglist.push_back(make_unique<DijkstraQ>());
	alglist.push_back(make_unique<IntersectionFast>());
	alglist.push_back(make_unique<Intersection>());
#if HAVE_CPLEX
	alglist.push_back(make_unique<LPFast>(tlimit));
	alglist.push_back(make_unique<LPOrig>(tlimit));
#endif
	
}

static inline void
print_usage(char *name)
{
	cerr << "Usage: " << name << " <seed> <npaths> <graph.lgf>" << endl;
}

int
main(int argc, char *argv[])
{
	lemon::Timer t;
	unsigned int seed, npaths;

	cerr << "CMC C++ Simulator 1.1" << endl;

	if (argc < 4) {
		print_usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	seed = strtol(argv[1], NULL, 10);
	npaths = strtol(argv[2], NULL, 10);

	if (npaths > MAX_PATHS) {
		cerr << "Max paths supported: " << MAX_PATHS << endl;
		exit(EXIT_FAILURE);
	}

	cerr << "Seed " << seed << " for graph " << argv[3] << endl;

	lemon::rnd.seed(seed);

	Simulator sim;
	sim.setup(argv[3], npaths);

	cerr << sim.flow_str() << endl;

	std::vector<std::unique_ptr<CMC>> alglist;
	init_alglist(alglist, npaths);

	for (auto & algorithm : alglist) {
		sim.run(*algorithm);
		cout << sim.result_message() << endl;
		if (sim.result_code() != Simulator::SUCCESS) {
			cerr << sim.error_message() << endl;
		}
	}

	alglist.clear();

	t.halt();
	cerr << "Simulation runtime: " << t.realTime() <<  endl;

	return 0;
}
