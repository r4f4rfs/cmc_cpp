#ifndef __FLOW_H__
#define __FLOW_H__

#include <sstream>
#include "graph.h"

namespace cmc {
	namespace concepts {
		class Flow {
			private:
				Digraph::Node source_, target_;
				unsigned char nclasses;
				unsigned char npaths;

			public:
				Flow() :
					source_(lemon::INVALID), target_(lemon::INVALID),
					nclasses(0), npaths(0) { }

				Flow(Digraph::Node s, Digraph::Node t,
						unsigned char nc, unsigned char np) :
					source_(s), target_(t), nclasses(nc), npaths(np) { }

				~Flow() { }

				inline std::string repr(const Digraph& graph) const {
					std::ostringstream ost;
					ost << "Flow (" << graph.id(source_) << ", " <<
						graph.id(target_) << ") " << (int)nclasses <<
						" classes " << (int)npaths << " paths";
					return ost.str();
				}

				inline void set_source(Digraph::Node source) {
					this->source_ = source;
				}

				inline void set_target(Digraph::Node target) {
					this->target_ = target;
				}

				inline void set_classes(unsigned char nc) {
					this->nclasses = nc;
				}

				inline void set_paths(unsigned char np) {
					this->npaths = np;
				}

				inline Digraph::Node source() const { return source_; }

				inline Digraph::Node target() const { return target_; }

				inline unsigned char classes() const { return nclasses; }

				inline unsigned char paths() const { return npaths; }
		};

	} /* namespace concepts */
} /* namespace cmc */

#endif /* __FLOW_H__ */
