#ifndef __LIGHTPATH_H__
#define __LIGHTPATH_H__

#include <lemon/path.h>

#include "common.h"

namespace cmc {
	namespace concepts {
		class Lightpath : public lemon::StaticPath<Digraph> {
			private:
				bitsetN classes_;

			public:
				Lightpath() : classes_(0) { }

				~Lightpath() { }

				Lightpath(const Lightpath& clpath) :
					lemon::StaticPath<Digraph>(clpath),
					classes_(clpath.classes_.to_ulong()) { }

				template<typename CPath >
				Lightpath(const CPath& p, const bitsetN cls) :
					lemon::StaticPath<Digraph>(p), classes_(cls.to_ulong()) { }

				Lightpath& operator=(const Lightpath& clpath) {
					lemon::StaticPath<Digraph>::operator=(clpath);
					classes_ = clpath.classes_;
					return *this;
				}

				template<typename CPath >
				Lightpath& operator=(const CPath& cpath) {
					lemon::StaticPath<Digraph>::operator=(cpath);
					classes_ = cpath.classes_;
					return *this;
				}

				bitsetN classes(void) const { return classes_; }

				unsigned long long cost(const Digraph& graph) const {
					unsigned long long c = 0;
					for (Lightpath::ArcIt a(*this); a != lemon::INVALID; ++a)
						c += graph.weight(a);
					return c;
				}

				void print(std::ostream& os, const Digraph& graph) {
					os << "[";
					for (lemon::PathNodeIt<Lightpath> n(graph, *this);
							n != lemon::INVALID; ++n)
						os << graph.id(n) << ",";
					os << "]";
				}
		};
	} /* namespace concepts */
} /* namespace cmc */

#endif /* __LIGHTPATH_H__ */
