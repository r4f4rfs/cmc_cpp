#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <lemon/core.h>
#include <lemon/maps.h>
#include <lemon/adaptors.h>
#include <lemon/list_graph.h>
#include <lemon/lgf_reader.h>

#include "common.h"

namespace cmc {
	namespace concepts {
		using namespace lemon;

		class Digraph : public ListDigraph {
			private:
				typedef Digraph::ArcMap<unsigned int> IntArcMap;
				IntArcMap weightMap;
				typedef Digraph::ArcMap<bitsetN> BitArcMap;
				BitArcMap classMap;

			public:
				Digraph() : weightMap(*this), classMap(*this) { }

				~Digraph() { }

				inline void load(const std::string &filename) {
					digraphReader(*this, filename)
						.arcMap("weight", weightMap)
						.arcMap("classes", classMap)
						.run();
				}

				inline const IntArcMap& weight_map(void) const {
					return weightMap;
				}

				inline const BitArcMap& class_map(void) const {
					return classMap;
				}

				inline unsigned int weight(Digraph::Arc a) const {
					return weightMap[a];
				}

				inline bitsetN classes(Digraph::Arc a) const {
					return classMap[a];
				}

#if 0
				inline IdMap<const Digraph, Digraph::Node> id_map() const {
					return IdMap<const Digraph, Digraph::Node>(*this);
				}
#endif


				friend std::ostream& operator<<(
						std::ostream& os, const Digraph::Arc& a) {
					return os << Digraph::id(a);
				}

				friend std::ostream& operator<<(
						std::ostream& os, const Digraph::Node& n) {
					return os << Digraph::id(n);
				}
		}; /* class Digraph */
	} /* namespace concepts */
} /* namespace cmc */


#endif /* __GRAPH_H__ */
