#ifndef __BACKTRACK_H__
#define __BACKTRACK_H__

#include "algorithms/cmc.h"
#include "concepts/lightpath.h"

namespace cmc {
	namespace algorithms {
		using namespace concepts;

		class Backtrack : public CMCSingle {
			public:
				Backtrack(unsigned long long limit = TIME_LIMIT) :
					CMCSingle(limit) { }

				~Backtrack() { }

				inline const std::string fullname() const {
					return "Branch and Bound";
				}

				inline const std::string abbrev_name() const {
					return "BranchBnd";
				}

				void solve(const Digraph& g, const Flow& f);
		};

		class MBacktrack : public CMCMulti {
			public:
				MBacktrack(unsigned long long limit = TIME_LIMIT, bool disjoint=true) :
					CMCMulti(limit, disjoint) { }

				~MBacktrack() { }

				inline const std::string fullname() const {
					return "Multipath Branch and Bound";
				}

				inline const std::string abbrev_name() const {
					return "MBBound";
				}

				void solve(const Digraph& g, const Flow& f);
		};

		class MBacktrackDFS : public CMCMulti {
			public:
				MBacktrackDFS(unsigned long long limit = TIME_LIMIT, bool disjoint=true) :
					CMCMulti(limit, disjoint) { }

				~MBacktrackDFS() { }

				inline const std::string fullname() const {
					return "Multiple Branch and Bound DFS";
				}

				inline const std::string abbrev_name() const {
					return "MBoundDFS";
				}

				void solve(const Digraph& g, const Flow& f);
		};
	} // namespace algorithms
} // namespace cmc

#endif /* __BACKTRACK_H__ */
