#include <vector>

#include <lemon/adaptors.h>
#include <lemon/dfs.h>
#include <lemon/dijkstra.h>

#include "intersection.h"

using namespace cmc;
using namespace algorithms;

typedef std::pair<unsigned char, unsigned long long> countPair;
typedef Digraph::ArcMap<unsigned int> LengthMap;

struct countPairSortPred {
	bool inline operator()(const countPair &left, const countPair &right) {
		return left.second > right.second;
	}
};

void
IntersectionFast::solve(const Digraph& graph, const Flow& flow)
{
	bitsetN final_classes(0);
	size_t nc = flow.classes();
	countPair classes_count[NCLASSES]; // Number of edges for each class

	for (int i = 0; i < NCLASSES; ++i)
		classes_count[i] = std::make_pair(i, 0);

	/* Count classes */
	for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a) {
		for (int i = 0; i < NCLASSES; ++i)
			classes_count[i].second += graph.classes(a).test(i);
	}

	/* Sort classes by number of edges in the induced subgraph */
	sort(std::begin(classes_count), std::end(classes_count), countPairSortPred());

	for (unsigned int i = 0; i < nc; ++i)
		final_classes.set(classes_count[i].first, 1);

	/* Not enough classes in the intersection */
	if (final_classes.count() < nc)
		return;

	/* Intersection */
	Digraph::ArcMap<bool> filter(graph, true);
	for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a)
		if ((graph.classes(a) & final_classes).count() < nc)
			filter[a] = false;

	/* Find shortest path in ther subgraphs intersection */
	lemon::FilterArcs<const Digraph> subgraph(graph, filter);
	lemon::Dijkstra<lemon::FilterArcs<const Digraph>, LengthMap > dijkstra(subgraph, graph.weight_map());
	dijkstra.run(flow.source(), flow.target());
	if (!dijkstra.reached(flow.target()))
		return;

	/* Create lightpath */
	const lemon::Path<Digraph>& p = dijkstra.path(flow.target());
	this->add_lightpath(p, final_classes, graph, flow);
}

void
Intersection::solve(const Digraph& graph, const Flow& flow)
{
	countPair glist[NCLASSES]; // Number of edges for each class
	unsigned long nc = flow.classes();

	bitsetN avail_classes = CMC::available_classes(graph, flow);
	for (int i = 0; i < NCLASSES; ++i) {
		glist[i] = std::make_pair(i, 0);

		/* This class cannot be used in any solution */
		if (!avail_classes.test(i))
			continue;

		/* Keep only arcs containing class i */
		Digraph::ArcMap<bool> filter(graph, true);
		for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a)
			if (!graph.classes(a).test(i))
				filter[a] = false;
		lemon::FilterArcs<const Digraph> subgraph(graph, filter);

		/* Check if there is a path between source and target */
		if (!lemon::dfs(subgraph).run(flow.source(), flow.target()))
			continue;

		glist[i].second = lemon::mapCount(graph, filter, true);
	}

	/* Sort subgraphs by number of edges per class */
	sort(std::begin(glist), std::end(glist), countPairSortPred());

	bitsetN final_classes(0);
	for (unsigned int i = 0; i < nc; ++i)
		final_classes.set(glist[i].first, 1);

	/* Not enough common classes */
	if (final_classes.count() < nc)
		return;

	/* Intersection */
	Digraph::ArcMap<bool> filter(graph, true);
	for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a) {
		if ((graph.classes(a) & final_classes) != final_classes)
			filter[a] = false;
	}
	lemon::FilterArcs<const Digraph> subinter(graph, filter);

	/* Find shortest path on the intersection of subgraphs */
	lemon::Dijkstra<lemon::FilterArcs<const Digraph>, LengthMap > dijkstra(subinter, graph.weight_map());
	dijkstra.run(flow.source(), flow.target());
	if (!dijkstra.reached(flow.target()))
		return;

	/* Create lightpath */
	this->add_lightpath(dijkstra.path(flow.target()), final_classes, graph, flow);
}
