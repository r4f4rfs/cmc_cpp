#ifndef __INTERSECTION_H__
#define __INTERSECTION_H__

#include <string>

#include "concepts/graph.h"
#include "concepts/lightpath.h"
#include "cmc.h"

namespace cmc {
	namespace algorithms {
		using namespace concepts;

		class IntersectionFast : public CMCSingle {
			public:
				IntersectionFast() { }

				void solve(const Digraph& graph, const Flow& flow);

				inline const std::string fullname() const {
					return "Intersection Fast";
				}

				inline const std::string abbrev_name() const {
					return "InterFast";
				}
		};

		class Intersection : public CMCSingle {
			public:
				Intersection() { }

				void solve(const Digraph& graph, const Flow& flow);

				inline const std::string fullname() const {
					return "Intersection";
				}

				inline const std::string abbrev_name() const {
					return "Intersect";
				}
		};
	}
}

#endif /* __INTERSECTION_H__ */
