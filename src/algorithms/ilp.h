#ifndef __ILP_H__
#define __ILP_H__

#include "cmc.h"

namespace cmc {
	namespace algorithms {
		using namespace concepts;

		class LPOrig : public CMCSingle {
			public:
				LPOrig(unsigned long long limit = TIME_LIMIT) :
					CMCSingle(limit) { }

				~LPOrig() { }

				inline const std::string fullname(void) const {
					return "Linear Programming Original";
				}

				inline const std::string abbrev_name(void) const {
					return "LPOrig";
				}

				void solve(const Digraph& g, const Flow& f);
		}; /* class LPOrig */

		class LPFast : public CMCSingle {
			public:
				LPFast(unsigned long long limit = TIME_LIMIT) :
					CMCSingle(limit) { }

				~LPFast() { }

				inline const std::string fullname(void) const {
					return "Linear Programming Fast";
				}

				inline const std::string abbrev_name(void) const {
					return "LPFast";
				}

				void solve(const Digraph& g, const Flow& f);
		}; /* class LPFast */

		class LPMulti : public CMCMulti {
			public:
				LPMulti(unsigned long long limit = TIME_LIMIT,
					bool arc_disjoint = true) :
					CMCMulti(limit, arc_disjoint) { }

				~LPMulti() { }

				inline const std::string fullname(void) const {
					return "Linear Programming Multipath";
				}

				inline const std::string abbrev_name(void) const {
					return "LPMulti";
				}

				void solve(const Digraph& g, const Flow& f);
		}; /* class LPMulti */
	}
}

#endif /* __ILP_H__ */
