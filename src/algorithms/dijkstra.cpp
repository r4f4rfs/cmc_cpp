#include <cmath>
#include <vector>
#include <climits>
#include <algorithm>

#include <lemon/dijkstra.h>
#include <lemon/fib_heap.h>
#include <lemon/bits/path_dump.h>

#include "dijkstra.h"

using namespace cmc;
using namespace concepts;
using namespace algorithms;

using std::vector;

typedef Digraph::NodeMap<Digraph::Arc> PredMap;
typedef Digraph::NodeMap<int> IntNodeMap;
typedef lemon::FibHeap<float, IntNodeMap > Heap;

static int
diameter(const Digraph& graph, const Digraph::Node& source, const Digraph::Node& target)
{
	lemon::Dijkstra<Digraph, Digraph::ArcMap<unsigned int> > dijkstra(graph, graph.weight_map());

	dijkstra.run(source);
	return dijkstra.reached(target) ? dijkstra.dist(target) : 0;
}

void
DijkstraX::solve(const Digraph& graph, const Flow& flow)
{
	PredMap predecessor(graph);
	Digraph::NodeMap<unsigned long long> distances(graph);
	IntNodeMap heapCrossRef(graph);
	Digraph::NodeMap<bitsetN> classes(graph);
	int source_diam = diameter(graph, flow.source(), flow.target());
	float sweight = (float)source_diam / (float)flow.classes();

	if (source_diam <= 0) { /* Target can't be reached from source */
		return;
	}

	/* Initialize SNodeT map */
	for (Digraph::NodeIt node(graph); node != lemon::INVALID; ++node) {
		distances[node] = INT_MAX;
		predecessor[node] = lemon::INVALID;
		heapCrossRef[node] = Heap::PRE_HEAP;
		classes[node] = bitsetN(0);
	}

	classes[flow.source()] = CMC::available_classes(graph, flow);
	distances[flow.source()] = 0;

	Heap queue(heapCrossRef);
	queue.push(flow.source(), 0.0);

	Digraph::Node node;
	while (!queue.empty()) {
		node = queue.top();
		queue.pop();

		if (node == flow.target())
			break;

		for (Digraph::OutArcIt a(graph, node); a != lemon::INVALID; ++a) {
			const Digraph::Node& t = graph.target(a);
			/* POST_HEAP: item left/not in the heap */
			if (queue.state(t) == Heap::POST_HEAP)
				continue;

			bitsetN c = graph.classes(a) & classes[node];
			if (c.count() < flow.classes()) {
				if (predecessor[t] != lemon::INVALID &&
					graph.source(predecessor[t]) == node)
					predecessor[t] = lemon::INVALID;
				continue;
			}

			// Update shortest distances, if needed
			unsigned int d = distances[node] + graph.weight(a);
			if (d < distances[t]) {
				distances[t] = d;
				predecessor[t] = a;
				classes[t] = c;
				//queue.set(t, this->score_fn(d, c.count()));
				queue.set(t, (float)d - sweight * c.count());
			}
		}
		node = lemon::INVALID;
	}

	// Free queue
	queue.clear();

	if (node != flow.target())
		return;

	const lemon::Path<Digraph>& path =
		lemon::PredMapPath<Digraph, PredMap>(graph, predecessor, flow.target());

	this->add_lightpath(path, classes[flow.target()], graph, flow);
}

#if 0
void
DijkstraT::solve(const Digraph& graph, const Flow& flow)
{
	PredMap predecessor(graph);
	Digraph::NodeMap<unsigned long long> distances(graph);
	IntNodeMap heapCrossRef(graph);
	Digraph::NodeMap<bitsetN> classes(graph);

	/* Initialize maps */
	for (Digraph::NodeIt v(graph); v != lemon::INVALID; ++v) {
		distances[v] = INT_MAX;
		predecessor[v] = lemon::INVALID;
		heapCrossRef[v] = Heap::PRE_HEAP;
		classes[v] = bitsetN(0);
	}

	Heap queue(heapCrossRef);
	classes[flow.source()] = CMC::available_classes(graph, flow);
	distances[flow.source()] = 0;
	queue.push(flow.source(), distances[flow.source()]);

	float min_mult = 1.5;
	//double dec = (std::ceil((min_mult - 1) / lemon::countNodes(graph)) * 10) / 10;
	double dec = 0.1;

	Digraph::Node node;
	bool added_child = false;
	while (!queue.empty()) {
		node = queue.top();
		queue.pop();

		if (node == flow.target())
			break;

		for (Digraph::OutArcIt a(graph, node); a != lemon::INVALID; ++a) {
			const Digraph::Node& neighbour = graph.target(a);
			if (queue.state(neighbour) == Heap::POST_HEAP)
				continue;

			bitsetN common = graph.classes(a) & classes[node];
			if (common.count() < flow.classes())
				continue;

			if (common.count() < std::ceil(min_mult * flow.classes())) {
				if (predecessor[neighbour] != lemon::INVALID &&
					graph.source(predecessor[neighbour]) == node)
					predecessor[neighbour] == lemon::INVALID;
				continue;
			}

			// Update shortest distances, if needed
			unsigned long long d = distances[node] + graph.weight(a);
			if (d < distances[neighbour]) {
				distances[neighbour] = d;
				predecessor[neighbour] = a;
				classes[neighbour] = common;
				queue.set(neighbour, d);
				added_child = true;
			}
		}
		min_mult = std::max(min_mult - dec, 1.0);
		if (min_mult == 1)
			added_child = true;
	}

	// Free queue
	queue.clear();

	if (node != flow.target())
		return;

	const lemon::Path<Digraph>& path =
		lemon::PredMapPath<Digraph, PredMap>(graph, predecessor, flow.target());

	this->add_lightpath(path, classes[node], graph, flow);
}
#endif

typedef Digraph::NodeMap<unsigned long long> LengthMap;
typedef Digraph::NodeMap<bitsetN> ClassMap;
typedef Digraph::NodeMap<bool> BoolMap;
typedef FilterNodes<const Digraph, BoolMap> FGraph;

Digraph::Node
DijkstraQ::min_node(const FGraph& graph, const LengthMap& distances,
		const BoolMap& discovered, const ClassMap& commons)
{
	std::vector<FGraph::Node> nodes;

	for (FGraph::NodeIt v(graph); v != lemon::INVALID; ++v) {
		if (discovered[v])
			nodes.push_back(v);
	}

	if (!nodes.size())
		return lemon::INVALID;

	std::random_shuffle(nodes.begin(), nodes.end());

	FGraph::Node &min = nodes[0];
	unsigned long long min_dist = INT_MAX;
	std::vector<FGraph::Node>::const_iterator it;
	for (it = nodes.begin(); it != nodes.end(); ++it) {
		const FGraph::Node& n = *it;
		if (distances[n] < min_dist) {
			min = n;
			min_dist = distances[n];
		}
		if (distances[n] < (total_dist / total_nodes) &&
			commons[n].count() > (size_t)(total_classes / total_nodes))
			return n;
	}
	return min;
}

void
DijkstraQ::solve(const Digraph& graph, const Flow& flow)
{
	BoolMap discovered(graph);
	BoolMap unknown(graph); // == !visited map
	PredMap predecessor(graph);
	LengthMap distances(graph);
	ClassMap commons(graph);

	/* Initialize maps */
	for (Digraph::NodeIt v(graph); v != lemon::INVALID; ++v) {
		discovered[v] = false;
		unknown[v] = true;
		predecessor[v] = lemon::INVALID;
		distances[v] = INT_MAX;
	}

	distances[flow.source()] = 0;
	commons[flow.source()] = CMC::available_classes(graph, flow);
	discovered[flow.source()] = true;

	this->total_nodes = 1;

	FGraph fg(graph, unknown);
	while (true) {
		const Digraph::Node& node = min_node(fg, distances, discovered, commons);
		if (node == lemon::INVALID)
			return;

		//visited[node] = true;
		if (total_dist > 0)
			total_dist -= distances[node];

		if (node == flow.target())
			break;

		for (FGraph::OutArcIt a(fg, node); a != lemon::INVALID; ++a) {
			const Digraph::Node& v = graph.target(a);
			//if (visited[v])
				//continue;

			const bitsetN& c = graph.classes(a) & commons[node];
			if (c.count() < (size_t)flow.classes()) {
				if (predecessor[v] != lemon::INVALID &&
					graph.source(predecessor[v]) == node)
					predecessor[v] = lemon::INVALID;
				continue;
			}

			unsigned long long d = distances[node] + graph.weight(a);
			unsigned int nclasses = commons[v].count();
			if (d < distances[v]) {
				if (discovered[v]) {
					total_dist -= distances[v];
					total_classes -= nclasses;
				} else {
					total_nodes++;
				}
				distances[v] = d;
				predecessor[v] = a;
				commons[v] = c;
				discovered[v] = true;
				total_dist += d;
				total_classes += nclasses;
			}
		}
		fg.disable(node);
	}

	if (commons[flow.target()].count() < flow.classes())
		return;

	const lemon::Path<Digraph>& path =
		lemon::PredMapPath<Digraph, PredMap>(graph, predecessor, flow.target());

	this->add_lightpath(path, commons[flow.target()], graph, flow);
}

Digraph::Node
DijkstraT::min_node(const FGraph& graph, const LengthMap& distances,
		const BoolMap& discovered, const ClassMap& commons, float t_param)
{
	std::vector<FGraph::Node> nodes;

	for (FGraph::NodeIt v(graph); v != lemon::INVALID; ++v) {
		if (discovered[v])
			nodes.push_back(v);
	}

	/* Try to find node with min dist and at least T * k colours */
	FGraph::Node min = lemon::INVALID, min_fail = lemon::INVALID;
	unsigned long long min_dist = INT_MAX, min_dist_t = INT_MAX;
	std::vector<FGraph::Node>::const_iterator it;
	for (it = nodes.begin(); it != nodes.end(); ++it) {
		const FGraph::Node& n = *it;
		unsigned long long d = distances[n];
		/* Considering all discovered nodes */
		if (d < min_dist) {
			min_fail = n;
			min_dist = d;
		}
		/* Considering only those with at least T * k colours */
		if ((commons[n].count() >= t_param) && (d < min_dist_t)) {
			min = n;
			min_dist_t = d;
		}
	}

	return min != lemon::INVALID ? min : min_fail;
}

void
DijkstraT::solve(const Digraph& graph, const Flow& flow)
{
	BoolMap discovered(graph);
	BoolMap unknown(graph); // == !visited map
	PredMap predecessor(graph);
	LengthMap distances(graph);
	ClassMap commons(graph);

	/* Initialize maps */
	for (Digraph::NodeIt v(graph); v != lemon::INVALID; ++v) {
		discovered[v] = false;
		unknown[v] = true;
		predecessor[v] = lemon::INVALID;
		distances[v] = INT_MAX;
	}

	distances[flow.source()] = 0;
	commons[flow.source()] = CMC::available_classes(graph, flow);
	discovered[flow.source()] = true;

	float t_param = this->default_t_param;
	float t_dec = t_param / lemon::countNodes(graph);

	FGraph fg(graph, unknown);
	while (true) {
		float param = t_param * flow.classes();
		const Digraph::Node& node = min_node(fg, distances, discovered, commons, param);
		if (node == lemon::INVALID)
			return;

		if (node == flow.target())
			break;

		for (FGraph::OutArcIt a(fg, node); a != lemon::INVALID; ++a) {
			const Digraph::Node& v = graph.target(a);

			const bitsetN& c = graph.classes(a) & commons[node];
			if (c.count() < (size_t)flow.classes()) {
				if (predecessor[v] != lemon::INVALID &&
					graph.source(predecessor[v]) == node)
					predecessor[v] = lemon::INVALID;
					continue;
			}

			unsigned long long d = distances[node] + graph.weight(a);
			if (d < distances[v]) {
				distances[v] = d;
				predecessor[v] = a;
				discovered[v] = true;
				commons[v] = c;
			}
		}
		fg.disable(node);
		t_param -= t_dec;
	}

	if (commons[flow.target()].count() < flow.classes())
		return;

	const lemon::Path<Digraph>& path =
		lemon::PredMapPath<Digraph, PredMap>(graph, predecessor, flow.target());

	this->add_lightpath(path, commons[flow.target()], graph, flow);
}
