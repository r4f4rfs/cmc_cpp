#include <vector>
#include <algorithm>
#include <queue>
#include <functional>

#include <lemon/dijkstra.h>
#include <lemon/adaptors.h>
#include <lemon/concepts/digraph.h>
#include <lemon/path.h>

#include "backtrack.h"

using namespace cmc;
using namespace concepts;
using namespace algorithms;

using std::vector;

typedef lemon::ReverseDigraph<const Digraph> RevGraph;
typedef Digraph::ArcMap<unsigned int> LengthMap;
typedef Digraph::Node Node;

class SearchNode : public lemon::SimplePath<Digraph> {
	private:
		Node source; // Small hack to work with lemon::Path
		bitsetN classes_;
		unsigned long long cost_;
		unsigned long long est_cost_;

	public:
		SearchNode() : source(lemon::INVALID), cost_(0), est_cost_(0) { }

		SearchNode(const Node& n, const bitsetN& cls, unsigned long long ec) :
			lemon::SimplePath<Digraph>(), source(n),
			classes_(cls.to_ulong()), cost_(0), est_cost_(ec) { }

		SearchNode(const SearchNode& sn) : lemon::SimplePath<Digraph>(sn) {
			source = sn.source;
			classes_ = bitsetN(sn.classes_.to_ulong());
			cost_ = sn.cost_;
			est_cost_ = sn.est_cost_;
		}

		~SearchNode() { }

		SearchNode operator=(const SearchNode& sn) {
			lemon::SimplePath<Digraph>::operator=(sn);
			this->source = sn.source;
			this->classes_ = sn.classes_;
			this->cost_ = sn.cost_;
			this->est_cost_ = sn.est_cost_;
			return *this;
		}

		bool operator<(const SearchNode& sn) const {
			long long res = est_cost_ - sn.est_cost_;
			return (res < 0) || (!res && this->length() < sn.length());
		}

		bool operator>(const SearchNode& sn) const {
			long long res = est_cost_ - sn.est_cost_;
			return (res > 0) || (!res && (this->length() > sn.length()));
		}

		void addBack(const Digraph::Arc& arc, const bitsetN& common,
				unsigned int weight, unsigned long long ec) {
			lemon::SimplePath<Digraph>::addBack(arc);
			this->cost_ += weight;
			this->est_cost_ = this->cost_ + ec;
			this->classes_ = bitsetN(common.to_ulong());
		}

		void eraseBack(const bitsetN& common, unsigned int weight, unsigned long long ec) {
			this->cost_ -= weight;
			this->est_cost_ = ec;
			this->classes_ = bitsetN(common.to_ulong());
			lemon::SimplePath<Digraph>::eraseBack();
		}

		/* Small hack to used lemon::Path for paths with a single node */
		inline Node target(const Digraph& graph) const {
			return this->empty() ? source : lemon::pathTarget(graph, *this);
		}

		inline bool has_node(const Node& node, const Digraph& graph) const {
			for (lemon::PathNodeIt<SearchNode > n(graph, *this); n != lemon::INVALID; ++n)
				if (node == n)
					return true;
			return false;
		}

		inline bool has_arc(const Digraph::Arc& arc) const {
			for (SearchNode::ArcIt a(*this); a != lemon::INVALID; ++a)
				if (arc == a)
					return true;
			return false;
		}

		inline bitsetN classes(void) const { return classes_; }

		inline unsigned long long cost(void) const { return cost_; }

		inline unsigned long long estimated_cost(void) const {
			return est_cost_;
		}

		static SearchNode extend(const SearchNode& sn, const Digraph::Arc& arc,
				const bitsetN& cls, unsigned int weight, unsigned long long ec) {
			SearchNode node(sn);
			node.addBack(arc, cls, weight, ec);
			return node;
		}
};

class MultiSearchNode {
	private:
		vector<SearchNode> snodes;

	public:
		MultiSearchNode() { }

		MultiSearchNode(int npaths, const Node& source,
				const bitsetN& classes, unsigned long long ec) :
			snodes(npaths, SearchNode(source, classes, ec)) {
		}

		MultiSearchNode(const MultiSearchNode& msn) {
			std::copy(msn.snodes.begin(), msn.snodes.end(),
					std::back_inserter(this->snodes));
		}

		~MultiSearchNode() { snodes.clear(); }

		MultiSearchNode operator=(const MultiSearchNode& msn) {
			this->snodes = msn.snodes;
			return *this;
		}

		const SearchNode& path(unsigned int path) const {
			return this->snodes[path];
		}

		inline bool has_node(unsigned int path, const Node& node,
				const Digraph& graph) const {
			return snodes[path].has_node(node, graph);
		}

		bool has_node(const Node& node, const Digraph& graph) const {
			auto has_node_fn = [&](const SearchNode& sn) {
				return sn.has_node(node, graph); };
			return std::any_of(snodes.begin(), snodes.end(), has_node_fn);
		}

		bool has_arc(const Digraph::Arc& a) const {
			auto has_arc_fn = [&](const SearchNode& sn) {
				return sn.has_arc(a); };
			return std::any_of(snodes.begin(), snodes.end(), has_arc_fn);
		}

		inline Node source(const Digraph& graph) const {
			return lemon::pathSource(graph, snodes[0]);
		}

		inline Node target(const Digraph& graph,
				unsigned int path) const {
			return this->snodes[path].target(graph);
		}

		unsigned int num_paths(void) const { return this->snodes.size(); }

		bitsetN classes(void) const {
			return std::accumulate(snodes.begin(), snodes.end(), bitsetN(0),
					[](const bitsetN& val, const SearchNode& sn) {
					return val | sn.classes(); });
		}

		unsigned int num_classes(void) const {
			return std::accumulate(snodes.begin(), snodes.end(), 0u,
					[](unsigned int val, const SearchNode& sn) {
					return val + sn.classes().count(); });
		}

		bitsetN classes_path(unsigned int path) const {
			return this->snodes[path].classes();
		}

		unsigned int num_classes_but_path(unsigned int path) const {
			return num_classes() - this->snodes[path].classes().count();
		}

		unsigned long long cost(void) const {
			return std::accumulate(snodes.begin(), snodes.end(), 0ull,
					[](unsigned long long val, const SearchNode& sn) {
					return val + sn.cost(); });
		}

		unsigned long long estimated_cost(void) const {
			return std::accumulate(snodes.begin(), snodes.end(), 0ull,
					[](unsigned long long val, const SearchNode& sn) {
					return val + sn.estimated_cost(); });
		}

		unsigned long long estimated_cost(unsigned int path) const {
			return this->snodes[path].estimated_cost();
		}

		bool reached_target(const Node& target, const Digraph& graph) const {
			auto reached_fn = [&target, &graph](const SearchNode& sn) {
				return target == sn.target(graph); };
			return std::all_of(snodes.begin(), snodes.end(), reached_fn);
		}

		bool operator<(const MultiSearchNode& msn) const {
			int res = estimated_cost() - msn.estimated_cost();
			return (res < 0) || (!res && this->snodes[0] < msn.snodes[0]);
		}

		bool operator>(const MultiSearchNode& msn) const {
			int res = estimated_cost() - msn.estimated_cost();
			return (res > 0) || (!res && this->snodes[0] > msn.snodes[0]);
		}

		void addBack(unsigned int path, const Digraph::Arc& arc,
				const bitsetN& common, unsigned int weight,
				unsigned long long estimated_cost) {
			this->snodes[path].addBack(arc, common, weight, estimated_cost);
		}

		void eraseBack(unsigned int path, const bitsetN& common,
				unsigned int weight, unsigned long long estimated_cost) {
			this->snodes[path].eraseBack(common, weight, estimated_cost);
		}

		static MultiSearchNode extend(const MultiSearchNode& msn, int path,
				const Digraph::Arc& arc, const bitsetN& common,
				unsigned int weight, unsigned long long ec) {
			MultiSearchNode m(msn);
			m.addBack(path, arc, common, weight, ec);
			return m;
		}
};

void
Backtrack::solve(const Digraph& graph, const Flow& flow)
{
	this->runtime.start();

	const Node& source = flow.source();
	const Node& target = flow.target();
	std::priority_queue<SearchNode, vector<SearchNode>, std::greater<SearchNode> > queue;

	RevGraph rg(graph);
	lemon::Dijkstra<RevGraph, LengthMap> dijkstra(rg, graph.weight_map());
	dijkstra.run(target);
	// No solution found
	if (!dijkstra.reached(source))
		return;

	const bitsetN avail_classes = CMC::available_classes(graph, flow);

	SearchNode actual(source, avail_classes, dijkstra.dist(source));
	queue.push(actual);

	while (!queue.empty() && (this->runtime.realTime() <= TIME_LIMIT)) {
		actual = queue.top();
		queue.pop();
		const Node& n = actual.target(graph);

		if (n == target)
			goto end;

		for (Digraph::OutArcIt a(graph, n); a != lemon::INVALID; ++a) {
			Node t = graph.target(a);

			/* Destination not reachable from node t */
			if (!dijkstra.reached(t))
				continue;

			bitsetN common = graph.classes(a) & actual.classes();
			if (common.count() < (size_t)flow.classes() ||
				actual.has_node(t, graph))
				continue;

			// Add neighbour to the queue
			queue.push(SearchNode::extend(actual, a, common, graph.weight(a),
						dijkstra.dist(t)));
		}
	}

	// No solution found
	this->runtime.halt();
	return;

end:
	this->add_lightpath(actual, actual.classes(), graph, flow);
	this->runtime.halt();
}

typedef lemon::Dijkstra<RevGraph, LengthMap> DijkstraMap;

static inline void
swap_best(MultiSearchNode** best, const MultiSearchNode& value)
{
	delete *best;
	*best = new MultiSearchNode(value);
}

static void
run_dfs(const Digraph& graph, const Flow& flow, MultiSearchNode** best_sn,
	MultiSearchNode& current, DijkstraMap& dijkstra, unsigned int path,
	lemon::Timer& runtime)
{

	if (runtime.realTime() > TIME_LIMIT)
		return;

	/* Already evaluated all paths */
	if (path > (unsigned int)flow.paths() - 1)
		return;

	const MultiSearchNode& best = **best_sn;
	bool best_reached = best.reached_target(flow.target(), graph);

	/* See if it's better than the current one */
	if (current.reached_target(flow.target(), graph)) {
		if (!best_reached || current.cost() < best.cost())
			swap_best(best_sn, current);
		return;
	}

	/* No reason to contine, since its worse than the current best */
	if (best_reached && (current.cost() > best.cost()))
		return;

	const Node& n = current.target(graph, path);

	/*
	 * This path reached the destination already, nothing else to do but
	 * to check the other paths
	 */
	if (n == flow.target()) {
		run_dfs(graph, flow, best_sn, current, dijkstra, path + 1, runtime);
		return;
	}

	for (Digraph::OutArcIt a(graph, n); a != lemon::INVALID; ++a) {
		const Node& t = graph.target(a);

		if (!dijkstra.reached(t) ||
			((t != flow.target() && current.has_node(path, t, graph))) ||
			current.has_arc(a))
			continue;

		bitsetN common = current.classes_path(path) & graph.classes(a);
		unsigned int total = common.count() + current.num_classes_but_path(path);

		if (!common.count() || (total < flow.classes()))
			continue;

		/* Save current values */
		bitsetN old_classes = current.classes_path(path);
		unsigned long long ec = current.estimated_cost(path);

		current.addBack(path, a, common, graph.weight(a), dijkstra.dist(t));

		run_dfs(graph, flow, best_sn, current, dijkstra, path, runtime);

		/* Restore old values */
		current.eraseBack(path, old_classes, graph.weight(a), ec);
	}
}

void
MBacktrackDFS::solve(const Digraph& graph, const Flow& flow)
{
	this->runtime.start();

	RevGraph rg(graph);
	DijkstraMap dijkstra(rg, graph.weight_map());
	dijkstra.run(flow.target());

	if (!dijkstra.reached(flow.source()))
		return;

	MultiSearchNode actual(flow.paths(), flow.source(),
			CMC::available_classes(graph, flow), dijkstra.dist(flow.source()));
	MultiSearchNode* best = new MultiSearchNode(actual);

	run_dfs(graph, flow, &best, actual, dijkstra, 0, this->runtime);
	this->runtime.halt();

	/* Solution found */
	if (best->reached_target(flow.target(), graph)) {
		unsigned int nclasses = flow.classes();
		for (int i = 0; i < flow.paths(); ++i) {
			bitsetN common(best->classes_path(i).to_ulong());
			if (!CMCMulti::fix_classes(common, nclasses, flow.paths()))
				break;
			this->add_lightpath(best->path(i), common, graph, flow);
			nclasses -= common.count();
		}
	}
	delete best;
}

void
MBacktrack::solve(const Digraph& graph, const Flow& flow)
{
	this->runtime.start();
	std::priority_queue<MultiSearchNode, vector<MultiSearchNode>, std::greater<MultiSearchNode> > queue;

	RevGraph rg(graph);
	lemon::Dijkstra<RevGraph, LengthMap> dijkstra(rg, graph.weight_map());
	dijkstra.run(flow.target());

	if (!dijkstra.reached(flow.source()))
		return;

	MultiSearchNode actual(flow.paths(), flow.source(),
			CMC::available_classes(graph, flow), dijkstra.dist(flow.source()));
	queue.push(actual);

	while (!queue.empty() && (this->runtime.realTime() <= TIME_LIMIT)) {
		actual = queue.top();
		queue.pop();

		/* Solution found */
		if (actual.reached_target(flow.target(), graph)) {
			unsigned int nclasses = flow.classes();
			for (int i = 0; i < flow.paths(); ++i) {
				bitsetN common(actual.classes_path(i).to_ulong());
				if (!CMCMulti::fix_classes(common, nclasses, flow.paths()))
					break;
				this->add_lightpath(actual.path(i), common, graph, flow);
				nclasses -= common.count();
			}
			return;
		}

		for (int path = 0; path < flow.paths(); ++path) {
			const Node& n = actual.target(graph, path);

			/*
			 * This path reached the destination already, nothing else to do
			 * but to check the other paths
			 */
			if (n == flow.target())
				continue;

			for (Digraph::OutArcIt a(graph, n); a != lemon::INVALID; ++a) {
				const Node& t = graph.target(a);

				if (!dijkstra.reached(t) ||
					((t != flow.target()) && actual.has_node(path, t, graph)) ||
					(this->arc_disjoint() && actual.has_arc(a)))
					continue;

				bitsetN common = actual.classes_path(path) & graph.classes(a);
				unsigned int total = common.count() + actual.num_classes_but_path(path);
				if (!common.count() || (total < flow.classes()))
					continue;

				queue.push(MultiSearchNode::extend(actual, path, a, common,
							graph.weight(a), dijkstra.dist(t)));
			}
		}
	}
	this->runtime.halt();
}
