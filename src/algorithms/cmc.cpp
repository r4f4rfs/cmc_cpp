#include "cmc.h"

using namespace cmc;
using namespace algorithms;

unsigned long long
CMC::preprocess(Digraph& graph, const Flow& flow)
{
	int np = flow.paths();
	size_t nclasses = flow.classes();
	bitsetN classes = CMC::available_classes(graph, flow);
	unsigned long long nrem = 0;

	/* Remove useless edges */
	for (Digraph::NodeIt u(graph); u != lemon::INVALID; ++u) {
		for (Digraph::NodeIt v(graph); v != lemon::INVALID; ++v) {
			Digraph::Arc a = lemon::findArc(graph, u, v);
			if (!graph.valid(a))
				continue;
			if ((graph.classes(a) & classes).count() < ((np > 1) ? 1 : nclasses)) {
				graph.erase(a);
				nrem++;
			}
		}
	}
	return nrem;
}

bitsetN
CMC::available_classes(const Digraph& graph, const Flow& flow)
{
	bitsetN avail_src(0); // No avail wlvs
	Digraph::Node source = flow.source();
	for (Digraph::OutArcIt i(graph, source); i != lemon::INVALID; ++i) {
		avail_src |= graph.classes(i);
		if (avail_src.count() == avail_src.size()) // All available
			break;
	}

	bitsetN avail_tgt(0);
	Digraph::Node target = flow.target();
	for (Digraph::InArcIt i(graph, target); i != lemon::INVALID; ++i) {
		avail_tgt |= graph.classes(i);
		if (avail_tgt.count() == avail_src.size())
			break;
	}

	return avail_src & avail_tgt;
}

bool
CMC::is_soluble(const Digraph& graph, const Flow& flow)
{
	int np = flow.paths();
	if ((lemon::countOutArcs(graph, flow.source()) < np) ||
		(lemon::countInArcs(graph, flow.target()) < np)) {
		std::cerr << "Not enough in/out arcs" << std::endl;
		return false;
	}

	if ((np == 1) && (CMC::available_classes(graph, flow).count() < (size_t)flow.classes())) {
		std::cerr << "Not enough common classes" << std::endl;
		return false;
	}

	return true;
}

/*
 * Check that the given lightpath is valid for the given flow
 */
void
CMC::check_lightpath(const lemon::Path<Digraph>& p, const bitsetN& cls,
		const Digraph& graph, const Flow& flow)
{
	bitsetN bs(~0); // All bits set
	std::ostringstream oss;
	const Digraph::Node& source = lemon::pathSource(graph, p);
	if (source != flow.source()) {
		oss << "Wrong source " << graph.id(source) << ". ";
		oss << "Expected " << graph.id(flow.source());
		oss << " instead.";
		throw error(oss);
	}
	const Digraph::Node& target = lemon::pathTarget(graph, p);
	if (target != flow.target()) {
		oss << "Wrong target " << graph.id(target) << ". ";
		oss << "Expected " << graph.id(flow.target()) << " ";
		oss << "instead.";
		throw error(oss);
	}
	// Make sure the path is valid
	if (!lemon::checkPath<Digraph, lemon::Path<Digraph> >(graph, p)) {
		oss << "Invalid path ";// p.print(oss, graph);
		throw error(oss);
	}
	Digraph::ArcMap<bitsetN> used_classes(graph);
	for (lemon::Path<Digraph>::ArcIt a(p); a != lemon::INVALID; ++a) {
		bitsetN inter = cls & used_classes[a];
		// Intersection should be empty
		if (inter.count()) {
			oss << "Reused classes " << inter;
			throw error(oss);
		}
		used_classes[a] |= cls;
		bs &= graph.classes(a);
	}
	if ((bs & cls).count() < (size_t) 1) {
		oss << "Wrong common classes " << bs;
		throw error(oss);
	}
}

bool
CMCSingle::fix_classes(bitsetN& common, unsigned int nclasses) {
	int to_remove = common.count() - nclasses;
	for (int i = common.size() - 1;
			i >= 0 && to_remove > 0; --i) {
		if (common.test(i)) {
			common.flip(i);
			to_remove--;
		}
	}
	return common.count() >= nclasses;
}

bool
CMCMulti::fix_classes(bitsetN& common, unsigned int nc, unsigned int np) {
	unsigned int i = lps.size();
	unsigned int req = std::min((nc - (np - i - 1)), (unsigned int)common.count());
	int off_count = common.count() - req;
	for (unsigned int j = 0; j < common.size() && (off_count > 0); ++j) {
		if (common.test(j)) {
			common.set(j, 0);
			--off_count;
		}
	}
	if (i == np)
		return nc == 0;
	return common.count() > 0;
}
