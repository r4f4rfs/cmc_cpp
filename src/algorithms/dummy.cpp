#include "cmc.h"

#include <cmath>
#include <vector>
#include <climits>
#include <algorithm>

#include <lemon/dijkstra.h>
#include <lemon/bin_heap.h>
#include <lemon/bits/path_dump.h>
#include <lemon/adaptors.h>

#include <lemon/time_measure.h>

#include "dummy.h"

using namespace cmc;
using namespace concepts;
using namespace algorithms;

using std::vector;

typedef Digraph::NodeMap<Digraph::Arc> PredMap;
typedef Digraph::NodeMap<int> IntNodeMap;
typedef lemon::BinHeap<unsigned long long, IntNodeMap > Heap;

static inline double
score_fn(unsigned long long dist, unsigned char common_classes) {
	 return dist + common_classes;
}

void
Dummy::solve(const Digraph& graph, const Flow& flow)
{
	this->runtime.start();

	PredMap predecessor(graph);
	Digraph::NodeMap<unsigned long long> distances(graph);
	IntNodeMap heapCrossRef(graph);
	Digraph::NodeMap<bitsetN> classes(graph);

	/* Initialize SNodeT map */
	for (Digraph::NodeIt node(graph); node != lemon::INVALID; ++node) {
		distances[node] = INT_MAX;
		predecessor[node] = lemon::INVALID;
		heapCrossRef[node] = Heap::PRE_HEAP;
		classes[node] = bitsetN(0);
	}

	classes[flow.source()] = CMC::available_classes(graph, flow);
	distances[flow.source()] = 0;

	Heap queue(heapCrossRef);
	queue.push(flow.source(), 0ll);

	Digraph::Node node;
	while (!queue.empty() && this->runtime.realTime() <= TIME_LIMIT) {
		node = queue.top();
		queue.pop();

		if (node == flow.target())
			break;

		for (Digraph::OutArcIt a(graph, node); a != lemon::INVALID; ++a) {
			const Digraph::Node& t = graph.target(a);
			/* POST_HEAP: item left/not in the heap */
			if (queue.state(t) == Heap::POST_HEAP)
				continue;

			bitsetN c = graph.classes(a) & classes[node];
			if (c.count() < flow.classes()) {
				if (predecessor[t] != lemon::INVALID &&
					graph.source(predecessor[t]) == node)
					predecessor[t] = lemon::INVALID;
				continue;
			}

			// Update shortest distances, if needed
			unsigned long long d = distances[node] + graph.weight(a);
			if (d < distances[t]) {
				distances[t] = d;
				predecessor[t] = a;
				classes[t] = c;
				queue.set(t, score_fn(d, c.count()));
			}
		}
		node = lemon::INVALID;
	}

	// Free queue
	queue.clear();
	this->runtime.halt();

	// Always return a blocked state
	return;
}
