#include "mcmcmin.h"

#include <algorithm>

#include <lemon/dijkstra.h>
#include <lemon/random.h>

using namespace cmc;
using namespace concepts;
using namespace algorithms;

typedef Digraph::ArcMap<unsigned int> LengthMap;
typedef lemon::FilterArcs<const Digraph> FilterGraph;

static void
common_classes(bitsetN& common, const Digraph::ArcMap<bitsetN>& classes,
		const lemon::Path<FilterGraph>& p)
{
	for (lemon::Path<FilterGraph>::ArcIt a(p); a != lemon::INVALID; ++a)
		common &= classes[a];
}

static void
remove_classes(Digraph::ArcMap<bitsetN>& classes, FilterGraph& graph,
		const lemon::Path<FilterGraph>& p, const bitsetN& common)
{
	for (lemon::Path<FilterGraph>::ArcIt a(p); a != lemon::INVALID; ++a) {
		classes[a] &= ~common;
		/* Remove arcs with no available classes */
		if (!classes[a].count())
			graph.disable(a);
	}
}

static void
remove_path(FilterGraph& graph, const lemon::Path<FilterGraph >& p)
{
	for (lemon::Path<FilterGraph>::ArcIt a(p); a != lemon::INVALID; ++a)
		graph.disable(a);
}

void
MCMCMin::solve(const Digraph& graph, const Flow& flow)
{
	/*
	 * A filter arc is an efficient procedure provided by the lemon library
	 * for "copying" the arcs of a graph when modifying this "copy" without
	 * actually modifying the original graph
	 */
	Digraph::ArcMap<bool> filter(graph, true);
	FilterGraph fgraph(graph, filter);

	/*
	 * Copy the class map since we need to modify it. If we modify the map here
	 * without copying it first, we'll have problems later when checking for
	 * validity of returned lightpaths.
	 */
	Digraph::ArcMap<bitsetN> classes(graph);
	lemon::mapCopy(graph, graph.class_map(), classes);

	unsigned int path = 0;
	unsigned int nclasses = flow.classes();
	while (path < flow.paths()) {
		lemon::Dijkstra<FilterGraph, LengthMap> dijkstra(fgraph, graph.weight_map());
		dijkstra.run(flow.source(), flow.target());
		if (!dijkstra.reached(flow.target()))
			break;

		const lemon::Path<FilterGraph >& p = dijkstra.path(flow.target());
		bitsetN common(~0);
		common_classes(common, classes, p);
		if (!CMCMulti::fix_classes(common, nclasses, flow.paths())) {
			// This only has effect on the non-disjoint case
			unsigned int i = 0;
			if (p.length() > 2)
				i = rnd[p.length() - 2] + 1;
			fgraph.disable(p.nth(i));
		}
		try {
			this->add_lightpath(p, common, graph, flow);
			if (this->arc_disjoint())
				remove_path(fgraph, p); /* arc-disjoint */
			else
				remove_classes(classes, fgraph, p, common);
			nclasses -= common.count();
			path++;
		} catch (CMC::error& error) {
			//throw error;
		}
	}
}
