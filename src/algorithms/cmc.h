#ifndef __CMC_H__
#define __CMC_H__

#include <vector>
#include <exception>
#include <stdexcept>

#include "concepts/graph.h"
#include "concepts/flow.h"
#include "concepts/lightpath.h"

#include <lemon/time_measure.h>

#define BLOCKED_STR " | 1 | [[]] | 100000 | 100000"

namespace cmc {
	namespace algorithms {
		using namespace concepts;

		class CMC {
			public:
				CMC(unsigned long long tlim = TIME_LIMIT) :
					runtime(false), tlimit(tlim) { }

				virtual ~CMC() { }

				virtual void solve(const Digraph& g, const Flow& f) = 0;


				virtual bool blocked(const Flow& flow) const = 0;

				virtual inline const std::string fullname() const {
					return "CMC Base";
				}

				virtual inline const std::string abbrev_name() const {
					return "CMC Base";
				}

				virtual void print(std::ostream&, const Digraph& graph,
						const Flow& flow) const = 0;

				/*
				 * Check that the given lightpath is valid for the given flow
				 */
				static void check_lightpath(const lemon::Path<Digraph>&,
						const bitsetN& cls, const Digraph&, const Flow&);

				static unsigned long long preprocess(Digraph& g, const Flow& flow);

				static bool is_soluble(const Digraph& g, const Flow& f);

				static bitsetN available_classes(const Digraph& g, const Flow& f);
			protected:
				lemon::Timer runtime;
				unsigned long long tlimit;
				
				virtual void add_lightpath(const lemon::Path<Digraph>&,
						const bitsetN&, const Digraph& graph, const Flow& flow) = 0;

			public:
				class error : virtual public std::runtime_error {

					public:
					explicit error(std::string& msg) :
						std::runtime_error(msg) { }

					explicit error(const char *msg) :
						std::runtime_error(msg) { }

					explicit error(std::ostringstream& oss) :
						std::runtime_error(oss.str().c_str()) { }

					~error() throw() { }

					virtual const char* what() const throw() {
						return std::runtime_error::what();
					}
				};
		}; /* class CMC */

		class CMCSingle : public CMC {
			private:
				// The solution (in case the algorithm has not blocked)
				const Lightpath *lp;

			protected:
				void add_lightpath(const lemon::Path<Digraph>& p,
						const bitsetN& cls, const Digraph& graph,
						const Flow& flow) {
					bitsetN common(cls.to_ulong());
					if (!CMCSingle::fix_classes(common, flow.classes())) {
						std::ostringstream oss;
						oss << "Not enough common classes " << common;
						throw CMC::error(oss);
					}
					CMC::check_lightpath(p, common, graph, flow);
					this->lp = new Lightpath(p, common);
				}

				const Lightpath& lightpath(void) const { return *lp; }

			public:
				CMCSingle(unsigned long long tlim = TIME_LIMIT) :
					CMC(tlim), lp(NULL) { }

				~CMCSingle() { if (this->lp) delete this->lp; }

				inline bool blocked(const Flow& flow) const {
					return !lp || !lp->length() ||
						(lp->classes().count() < flow.classes());
				}

				void print(std::ostream& os, const Digraph& graph,
						const Flow& flow) const {
					os << abbrev_name();
					if (blocked(flow)) {
						os << BLOCKED_STR;
						return;
					}
					os << " | 0 | [[";
					lemon::PathNodeIt<Lightpath> n(graph, *lp);
					for ( ; n != lemon::INVALID; ++n)
						os << graph.id(n) << ",";
					os << " (" << lp->classes() << ")]] | " << lp->cost(graph);
				}

				static bool fix_classes(bitsetN& common, unsigned int nclasses);
		}; /* class CMCSingle */

		class CMCMulti : public CMC {
			private:
				//unsigned int nc;
				// The solution (in case the algorithm has not blocked)
				std::vector<Lightpath> lps;
				// Indicates whether only arc-disjoint paths should be
				// considered in the solution
				bool only_arc_disjoint;

			protected:
				void add_lightpath(const lemon::Path<Digraph>& p,
						const bitsetN& cls,
						const Digraph& graph, const Flow& flow) {
					CMC::check_lightpath(p, cls, graph, flow);
					lps.push_back(Lightpath(p, cls));
				}

				const std::vector<Lightpath>& lightpaths(void) const {
					return lps;
				}

				inline bool arc_disjoint(void) const {
					return only_arc_disjoint;
				}

			public:
				CMCMulti(unsigned long long tlim = TIME_LIMIT, bool arc_disjoint = true) :
					CMC(tlim), only_arc_disjoint(arc_disjoint) { }

				~CMCMulti() { }

				inline bool blocked(const Flow& flow) const {
					if (lps.size() != flow.paths())
						return true;
					unsigned int count = 0;
					std::vector<Lightpath>::const_iterator it;
					for (it = lps.begin(); it != lps.end(); ++it)
						count += it->classes().count();
					return count < flow.classes();
				}

				void print(std::ostream& os, const Digraph& graph,
						const Flow& flow) const {
					os << abbrev_name();
					if (blocked(flow)) {
						os << BLOCKED_STR;
						return;
					}
					os << " | 0 | [";
					std::vector<Lightpath>::const_iterator it;
					unsigned long long cost = 0;
					for (it = lps.begin(); it != lps.end(); ++it) {
						cost += it->cost(graph);
						os << "[";
						lemon::PathNodeIt<Lightpath> n(graph, *it);
						for ( ; n != lemon::INVALID; ++n)
							os << graph.id(n) << ",";
						os << " (" << it->classes() << ")]";
					}
					os << "] | " << cost;
				}

				bool fix_classes(bitsetN& common, unsigned int nclasses,
						unsigned int npaths);
		}; /* class CMCMulti */
	} /* namespace algorithms */
} /* namespace cmc */

#endif /* __CMC_H__ */
