extern "C" {
	#include <ilcplex/cplex.h>
}

#include <algorithm>
#include <lemon/cplex.h>
#include "ilp.h"
#include <lemon/cplex.h>

static const char *SolutionType[] = {
	"UNDEFINED", "INFEASIBLE", "FEASIBLE", "OPTIMAL", "UNBOUNDED"
};

using namespace cmc;
using namespace concepts;
using namespace algorithms;

using lemon::CplexMip;

typedef std::pair<Digraph::Arc, unsigned char> ArcClassPair;

static bool
extract_path_orig(const Digraph& graph, const Flow& flow, const CplexMip& mip,
			std::map<ArcClassPair, CplexMip::Col>& class_arc_var,
			Path<Digraph>& p)
{
	Digraph::Node next, cur = flow.source();

	while (cur != flow.target()) {
		std::cerr << "Cur " << graph.id(cur) << std::endl;
		next = cur;
		for (Digraph::OutArcIt a(graph, cur); a != lemon::INVALID; ++a) {
			std::cerr << "(" << graph.id(a);
			for (unsigned char i = 0; i < NCLASSES; ++i) {
				if (mip.sol(class_arc_var[std::make_pair(a, i)]) > 0) {
					std::cerr << ", " << (int)i << " [" << mip.sol(class_arc_var[std::make_pair(a, i)]) << "]";
				}
			}
			std::cerr << ")" << std::endl;
		}
		for (Digraph::OutArcIt a(graph, cur); a != lemon::INVALID; ++a) {
			bool arc_in_sol = false;
			for (unsigned char i = 0; i < NCLASSES; ++i) {
				if (mip.sol(class_arc_var[std::make_pair(a, i)]) > 0) {
					arc_in_sol = true;
					break;
				}
			}
			if (arc_in_sol) {
				next = graph.target(a);
				p.addBack(a);
				break;
			}
		}
		if (next == cur)
			return false;
		cur = next;
	}

	return true;
}

static bool
extract_path(const Digraph& graph, const Flow& flow, const CplexMip& mip,
		const Digraph::ArcMap<CplexMip::Col>& arc_var, Path<Digraph>& p)
{
	Digraph::Node next, cur = flow.source();

	while (cur != flow.target()) {
		next = cur;
		for (Digraph::OutArcIt a(graph, cur); a != lemon::INVALID; ++a) {
			if (mip.sol(arc_var[a]) > 0) {
				next = graph.target(a);
				p.addBack(a);
				break;
			}
		}
		if (next == cur)
			return false;
		cur = next;
	}

	return true;
}

static bool
extract_classes(const Flow& flow, const CplexMip& mip,
		std::map<unsigned char, CplexMip::Col>& class_var, bitsetN& classes)
{
	classes.reset();

	for (unsigned char i = 0; i < NCLASSES; ++i) {
		if (mip.sol(class_var[i]) > 0)
			classes.set(i, 1);
	}

	return classes.count() == flow.classes();
}

static bool
load_model(const Digraph& graph, const Flow& flow, CplexMip& mip,
		std::map<unsigned char, CplexMip::Col>& class_var,
		Digraph::ArcMap<CplexMip::Col>& arc_var,
		const lemon::Timer& t, const unsigned int tlimit)
{

	/* Variable that indicates if a class is used in that arc */
	std::map<ArcClassPair, CplexMip::Col> class_arc_var;
	for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a) {
		if (t.realTime() > tlimit)
			return false;

		for (unsigned char i = 0; i < NCLASSES; ++i)
			class_arc_var[std::make_pair(a, i)] = mip.addCol();
	}
	mip.addColSet(class_arc_var);
	mip.colBounds(class_arc_var, 0, 1);

	mip.addColSet(arc_var);
	mip.colBounds(arc_var, 0, 1);

	/* Variable that indicates if a class is used in the solution */
	for (unsigned char i = 0; i < NCLASSES; ++i)
		class_var[i] = mip.addCol();
	mip.addColSet(class_var);
	mip.colBounds(class_var, 0, 1);

	for (CplexMip::ColIt col(mip); col != lemon::INVALID; ++col) {
		if (t.realTime() > tlimit)
			return false;
		mip.colType(col, CplexMip::INTEGER);
	}

	/* Flow conservation constraints */
	for (Digraph::NodeIt node(graph); node != lemon::INVALID; ++node) {
		if (t.realTime() > tlimit)
			return false;

		CplexMip::Expr e1;
		for (Digraph::OutArcIt a(graph, node); a != lemon::INVALID; ++a)
			e1 += arc_var[a];
		mip.addRow(e1 <= 1);

		for (unsigned char i = 0; i < NCLASSES; ++i) {
			CplexMip::Expr e;
			for (Digraph::OutArcIt a(graph, node); a != lemon::INVALID; ++a)
				e += class_arc_var[std::make_pair(a, i)];
			for (Digraph::InArcIt a(graph, node); a != lemon::INVALID; ++a)
				e -= class_arc_var[std::make_pair(a, i)];

			if (node == flow.source())
				mip.addRow(e - class_var[i] == 0);
			else if (node == flow.target())
				mip.addRow(e + class_var[i] == 0);
			else
				mip.addRow(e == 0);
		}
	}

	/* Required wavelengths constraint */
	CplexMip::Expr e;
	for (unsigned char i = 0; i < NCLASSES; ++i)
		e += class_var[i];
	mip.addRow(e == flow.classes());

	/* Objective function */
	CplexMip::Expr obj;
	/* Continuity constraint */
	for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a) {
		if (t.realTime() > tlimit)
			return false;

		CplexMip::Expr e;
		for (unsigned char i = 0; i < NCLASSES; ++i) {
			const ArcClassPair pair = std::make_pair(a, i);
			mip.addRow(class_arc_var[pair] <= arc_var[a] * graph.classes(a).test(i));
			mip.addRow(class_arc_var[pair] <= graph.classes(a).test(i));
			e += graph.classes(a).test(i) * class_arc_var[pair];
			obj += graph.weight(a) * class_arc_var[pair];
		}
		mip.addRow(e == arc_var[a] * flow.classes());
	}

	mip.min();
	mip.obj(obj);

	return true;
}

void
LPOrig::solve(const Digraph& graph, const Flow& flow)
{
	CplexMip mip;

	this->runtime.start();

	/* Turn off debugging messages */
	mip.messageLevel(CplexMip::MESSAGE_NOTHING);
	//mip.messageLevel(CplexMip::MESSAGE_VERBOSE);

	/* Variable that indicates if a class is used */
	std::map<unsigned char, CplexMip::Col> class_var;

	/* Variable that indicates if an arc is used in the solution */
	Digraph::ArcMap<CplexMip::Col> arc_var(graph);

	std::cerr << "Loading model... ";
	//auto dur = measure<>::execution(load_model, graph, flow, mip, class_var, arc_var);
	//std::cerr << runtime << std::endl;
	if (!load_model(graph, flow, mip, class_var, arc_var, this->runtime, this->tlimit)) {
		std::cerr << "Time out " << this->runtime.realTime() << std::endl;
		return;
	}
	std::cerr << this->runtime.realTime() << std::endl;

	auto tlim = std::max(0ll, (long long)this->tlimit - (long long)(this->runtime.realTime()));
	CPXsetdblparam(mip.cplexEnv(), CPX_PARAM_TILIM, tlim);
	std::cerr << "Time limit: " << tlim << std::endl;

	int res = mip.solve();
	int type = mip.type();
	this->runtime.halt();

	if (res == CplexMip::UNSOLVED) {
		std::cerr << "LPOrig unsolvable: " << SolutionType[type] << std::endl;
		return;
	}

	if (type != CplexMip::OPTIMAL && type != CplexMip::FEASIBLE) {
		std::cerr << "LPOrig possible but " << SolutionType[type] << std::endl;
		return;
	}

	lemon::Path<Digraph> p;
	if (!extract_path(graph, flow, mip, arc_var, p)) {
		std::cerr << "LPOrig could not extract paths" << std::endl;
		return;
	}
	bitsetN common;
	if (!extract_classes(flow, mip, class_var, common)) {
		std::cerr << "LPOrig could not assigned classes" << std::endl;
		return;
	}

	this->add_lightpath(p, common, graph, flow);
}

static bool
load_model_fast(const Digraph& graph, const Flow& flow, CplexMip& mip,
		std::map<unsigned char, CplexMip::Col>& class_var,
		Digraph::ArcMap<CplexMip::Col>& arc_var, const lemon::Timer& time,
		unsigned int tlimit)
{
	/* Variable that indicates whether an arc is used in the solution */
	mip.addColSet(arc_var);
	mip.colBounds(arc_var, 0, 1);

	for (unsigned char i = 0; i < NCLASSES; ++i)
		class_var[i] = mip.addCol();
	mip.addColSet(class_var);
	mip.colBounds(class_var, 0, 1);

	for (CplexMip::ColIt col(mip); col != lemon::INVALID; ++col)
		mip.colType(col, CplexMip::INTEGER);

	if (time.realTime() > tlimit)
		return false;

	/* Flow conservation constraint */
	for (Digraph::NodeIt node(graph); node != lemon::INVALID; ++node) {
		if (time.realTime() > tlimit)
			return false;

		CplexMip::Expr e;
		for (Digraph::OutArcIt a(graph, node); a != lemon::INVALID; ++a)
			e += arc_var[a];
		for (Digraph::InArcIt a(graph, node); a != lemon::INVALID; ++a)
			e -= arc_var[a];

		if (node == flow.source())
			mip.addRow(e == 1);
		else if (node == flow.target())
			mip.addRow(e == -1);
		else
			mip.addRow(e == 0);
	}

	if (time.realTime() > tlimit)
		return false;

	/* Classes requirenment constraint */
	CplexMip::Expr e;
	for (unsigned char i = 0; i < NCLASSES; ++i)
		e += class_var[i];
	mip.addRow(e == flow.classes());

	if (time.realTime() > tlimit)
		return false;

	/* Object function */
	CplexMip::Expr obj;

	/* Continuity constraint */
	for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a) {
		if (time.realTime() > tlimit)
			return false;

		CplexMip::Expr e;
		for (unsigned char i = 0; i < NCLASSES; ++i)
			e += graph.classes(a).test(i) * class_var[i];
		mip.addRow(e >= arc_var[a] * flow.classes());
		obj += graph.weight(a) * arc_var[a];
	}

	mip.min();
	mip.obj(obj);

	return true;
}

void
LPFast::solve(const Digraph& graph, const Flow& flow)
{
	this->runtime.start();

	CplexMip mip;

	mip.messageLevel(CplexMip::MESSAGE_NOTHING);

	Digraph::ArcMap<CplexMip::Col> arc_var(graph);
	std::map<unsigned char, CplexMip::Col> class_var;

	std::cerr << "Loading model... ";
	if (!load_model_fast(graph, flow, mip, class_var, arc_var, this->runtime, this->tlimit)) {
		std::cerr << "Time out " << this->runtime.realTime() <<  std::endl;
		return;
	}
	std::cerr << this->runtime.realTime() << std::endl;

	auto tlim = std::max(0ll, (long long)this->tlimit - (long long)this->runtime.realTime());
	std::cerr << "Time limit: " << tlim << std::endl;

	CPXsetdblparam(mip.cplexEnv(), CPX_PARAM_TILIM, tlim);

	int res = mip.solve();
	int type = mip.type();

	this->runtime.halt();

	if (res == CplexMip::UNSOLVED) {
		std::cerr << "Unsolved LPFast: " << SolutionType[type] << std::endl;
		return;
	}

	if (type != CplexMip::OPTIMAL && type != CplexMip::FEASIBLE) {
		std::cerr << "LPFast possible but " << SolutionType[type] << std::endl;
		return;
	}

	lemon::Path<Digraph> p;
	if (!extract_path(graph, flow, mip, arc_var, p)) {
		std::cerr << "LPFast could not extract paths" << std::endl;
		return;
	}
	bitsetN common;
	if (!extract_classes(flow, mip, class_var, common)) {
		std::cerr << "LPFast could not assigned classes" << std::endl;
		return;
	}

	this->add_lightpath(p, common, graph, flow);
}

typedef std::pair<unsigned char, unsigned char> PathClassPair;
typedef std::pair<Digraph::Arc, unsigned char> ArcPathPair;
typedef std::tuple<Digraph::Arc, unsigned char, unsigned char> ArcPathClassTuple;

static bool
load_model_multi(const Digraph& graph, const Flow& flow, CplexMip& mip,
		std::map<ArcPathPair, CplexMip::Col>& path_arc_var,
		std::map<PathClassPair, CplexMip::Col>& path_class_var,
		bool is_arc_disjoint, const lemon::Timer& runtime, unsigned int tlimit)
{
	/* Variable that indicates whether class is used in that arc */
	for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a) {
		if (runtime.realTime() > tlimit)
			return false;
		for (unsigned char p = 0; p < flow.paths(); ++p)
			path_arc_var[std::make_pair(a, p)] = mip.addCol();
	}
	mip.addColSet(path_arc_var);
	mip.colBounds(path_arc_var, 0, 1);

	if (runtime.realTime() > tlimit)
		return false;

	/* Variable that indicates if that classes is used in that arc of
	 * that path
	 */
	std::map<ArcPathClassTuple, CplexMip::Col> path_class_arc_var;
	for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a) {
		if (runtime.realTime() > tlimit)
			return false;

		for (unsigned char p = 0; p < flow.paths(); ++p) {
			for (unsigned char i = 0; i < NCLASSES; ++i)
				path_class_arc_var[std::make_tuple(a, p, i)] = mip.addCol();
		}
	}
	mip.addColSet(path_class_arc_var);
	mip.colBounds(path_class_arc_var, 0, 1);

	/* Variable that indicates whether a classes is used in that path */
	for (unsigned char p = 0; p < flow.paths(); ++p) {
		for (unsigned char i = 0; i < NCLASSES; ++i)
			path_class_var[std::make_pair(p, i)] = mip.addCol();
	}
	mip.addColSet(path_class_var);
	mip.colBounds(path_class_var, 0, 1);

	if (runtime.realTime() > tlimit)
		return false;

	for (CplexMip::ColIt col(mip); col != lemon::INVALID; ++col)
		mip.colType(col, CplexMip::INTEGER);

	if (runtime.realTime() > tlimit)
		return false;

	/* Flow conservation constraint */
	for (Digraph::NodeIt node(graph); node != lemon::INVALID; ++node) {
		if (runtime.realTime() > tlimit)
			return false;

		for (unsigned char p = 0; p < flow.paths(); ++p) {
			/*
			CplexMip::Expr e;
			for (Digraph::OutArcIt a(graph, node); a != lemon::INVALID; ++a)
				e += path_arc_var[std::make_pair(a, p)];
			for (Digraph::InArcIt a(graph, node); a != lemon::INVALID; ++a)
				e -= path_arc_var[std::make_pair(a, p)];

			if (node == flow.source())
				mip.addRow(e == 1);
			else if (node == flow.target())
				mip.addRow(e == -1);
			else
				mip.addRow(e == 0);
			*/

			for (unsigned char i = 0; i < NCLASSES; ++i) {
				CplexMip::Expr exp;
				const PathClassPair cpair = std::make_pair(p, i);
				for (Digraph::OutArcIt a(graph, node); a != lemon::INVALID; ++a)
					exp += path_class_arc_var[std::make_tuple(a, p, i)];
				for (Digraph::InArcIt a(graph, node); a != lemon::INVALID; ++a)
					exp -= path_class_arc_var[std::make_tuple(a, p, i)];

				if (node == flow.source())
					mip.addRow(exp - path_class_var[cpair] == 0);
				else if (node == flow.target())
					mip.addRow(exp + path_class_var[cpair] == 0);
				else
					mip.addRow(exp == 0);
			}
		}
	}

	/* Required classes constraint */
	CplexMip::Expr e;
	for (unsigned char p = 0; p < flow.paths(); ++p) {
		CplexMip::Expr e1;
		for (unsigned char i = 0; i < NCLASSES; ++i) {
			const PathClassPair pair = std::make_pair(p, i);
			e += path_class_var[pair];
			e1 += path_class_var[pair];
		}
		/* At least one class must be used in each path */
		mip.addRow(e1 >= 1);
	}
	mip.addRow(e == flow.classes());

	if (runtime.realTime() > tlimit)
		return false;

	/* Objective function */
	CplexMip::Expr obj;

	/* Continuity constraint */
	for (Digraph::ArcIt a(graph); a != lemon::INVALID; ++a) {
		if (runtime.realTime() > tlimit)
			return false;

		/* Compatible paths */
		for (unsigned char i = 0; i < NCLASSES; ++i) {
			CplexMip::Expr e;
			for (unsigned char p = 0; p < flow.paths(); ++p)
				e += path_class_arc_var[std::make_tuple(a, p, i)];
			mip.addRow(e <= 1);
		}

		CplexMip::Expr e, de;
		for (unsigned char p = 0; p < flow.paths(); ++p) {
			/* disjoint arcs */
			de += path_arc_var[std::make_pair(a, p)];

			const ArcPathPair pair = std::make_pair(a, p);
			for (unsigned char i = 0; i < NCLASSES; ++i) {
				const ArcPathClassTuple tuple = std::make_tuple(a, p, i);
				const PathClassPair pc = std::make_pair(p, i);

				mip.addRow(path_class_arc_var[tuple] <=
						path_arc_var[pair] * graph.classes(a).test(i));
				mip.addRow(path_class_arc_var[tuple] * graph.classes(a).test(i) <= path_arc_var[pair]);
				mip.addRow(path_class_arc_var[tuple] <= path_class_var[pc] * graph.classes(a).test(i));
				mip.addRow(path_class_arc_var[tuple] * graph.classes(a).test(i) <= path_class_var[pc]);
				mip.addRow(path_class_arc_var[tuple] <= graph.classes(a).test(i));
			}
			obj += path_arc_var[pair] * graph.weight(a);
		}
		if (is_arc_disjoint) {
			mip.addRow(de <= 1);
		}
	}

	mip.min();
	mip.obj(obj);

	return true;
}

void
LPMulti::solve(const Digraph& graph, const Flow& flow)
{
	this->runtime.start();

	CplexMip mip;

	mip.messageLevel(CplexMip::MESSAGE_NOTHING);
	//mip.messageLevel(CplexMip::MESSAGE_VERBOSE);

	std::map<ArcPathPair, CplexMip::Col> path_arc_var;
	std::map<PathClassPair, CplexMip::Col> path_class_var;

	std::cerr << "Loading model... ";
	//std::cerr << measure<>::execution(load_model_multi, graph, flow, mip, path_arc_var, path_class_var, this->arc_disjoint()) << std::endl;
	if (!load_model_multi(graph, flow, mip, path_arc_var, path_class_var, this->arc_disjoint(), this->runtime, this->tlimit)) {
		std::cerr << "Time out " << this->runtime.realTime() << std::endl;
		return;
	}
	std::cerr << this->runtime.realTime() << std::endl;

	auto tlim = std::max(0ll, (long long)this->tlimit - (long long)(this->runtime.realTime()));

	CPXsetdblparam(mip.cplexEnv(), CPX_PARAM_TILIM, tlim);
	std::cerr << "Time limit: " << tlim << std::endl;

	int res = mip.solve();
	int type = mip.type();

	this->runtime.halt();

	if (res == CplexMip::UNSOLVED) {
		std::cerr << "Unsolved LPMulti: " << SolutionType[type] << std::endl;
		return;
	}

	if (type != CplexMip::OPTIMAL && type != CplexMip::FEASIBLE) {
		std::cerr << "Possible LPMulti but: " << SolutionType[type] << std::endl;
		return;
	}

	/* Extract paths */
	for (unsigned char p = 0; p < flow.paths(); ++p) {
		Digraph::Node next;
		Digraph::Node cur = flow.source();
		Path<Digraph> path;

		/* Extract path */
		while (cur != flow.target()) {
			next = cur;
			for (Digraph::OutArcIt a(graph, cur); a != lemon::INVALID; ++a) {
				if (mip.sol(path_arc_var[std::make_pair(a, p)]) > 0) {
					next = graph.target(a);
					path.addBack(a);
					break;
				}
			}
			if (next == cur)
				return;
			cur = next;
		}

		/* Extract classes */
		bitsetN common(0);
		for (unsigned char i = 0; i < NCLASSES; ++i) {
			if (mip.sol(path_class_var[std::make_pair(p, i)]) > 0)
				common.set(i, 1);
		}
		this->add_lightpath(path, common, graph, flow);
	}
}
