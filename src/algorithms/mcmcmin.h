#ifndef __MCMCMIN_H__
#define __MCMCMIN_H__

#include "cmc.h"

namespace cmc {
	namespace algorithms {
		using namespace concepts;

		class MCMCMin : public CMCMulti {
			public:
				MCMCMin(unsigned long long limit = TIME_LIMIT, bool disjoint = true) :
					CMCMulti(limit, disjoint) { }

				~MCMCMin() { }

				void solve(const Digraph& graph, const Flow& flow);

				inline const std::string fullname(void) const {
					return "MCMC Shortest Paths";
				}

				inline const std::string abbrev_name(void) const {
					return "MCMCMin";
				}
		};
	} /* namespace algorithms */
} /* namespace cmc */
#endif /* __MCMCMIN_H__ */
