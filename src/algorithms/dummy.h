#ifndef __DUMMY_H__
#define __DUMMY_H__

#include "cmc.h"

namespace cmc {
	namespace algorithms {
		using namespace concepts;

		class Dummy : public CMCSingle {
			public:
				Dummy(unsigned long long limit = TIME_LIMIT) :
					CMCSingle(limit) { }

				~Dummy() { }

				void solve(const Digraph& graph, const Flow& flow);

				const std::string fullname(void) const {
					return "Dummy Algorithm";
				}	

				const std::string abbrev_name(void) const {
					return "DummyAlgo";
				}
		};
	} /* namespace algorithms */
} /* namespace cmc */

#endif /* __DUMMY_H__ */
