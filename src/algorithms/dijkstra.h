#ifndef __DIJKSTRA_H__
#define __DIJKSTRA_H__

#include "cmc.h"

#include <lemon/dijkstra.h>
#include <lemon/adaptors.h>

typedef double (*ScoreFunction)(unsigned long long, unsigned char);

namespace cmc {
	namespace algorithms {
		using namespace concepts;

		class DijkstraQ : public CMCSingle {
			public:
				DijkstraQ() : total_nodes(0), total_dist(0), total_classes(0) { }

				~DijkstraQ() { }

				void solve(const Digraph& graph, const Flow& flow);

				const std::string fullname(void) const {
					return "Dijkstra Quadrant";
				}

				const std::string abbrev_name(void) const {
					return "DijkstraQ";
				}

			private:
				typedef Digraph::NodeMap<bool> BoolMap;
				unsigned int total_nodes, total_dist, total_classes;

				Digraph::Node min_node(
						const FilterNodes<const Digraph, BoolMap >& graph,
						const Digraph::NodeMap<unsigned long long>& distances,
						const Digraph::NodeMap<bool>& discovered,
						const Digraph::NodeMap<bitsetN>& common);
		};

		class DijkstraT : public CMCSingle {
			public:
				DijkstraT() { }

				~DijkstraT() { }

				void solve(const Digraph& graph, const Flow& flow);

				const std::string fullname(void) const {
					return "Dijkstra T";
				}

				const std::string abbrev_name(void) const {
					return "DijkstraT";
				}

			private:
				typedef Digraph::NodeMap<bool> BoolMap;
				const float default_t_param = 1.5;

				Digraph::Node min_node(
						const FilterNodes<const Digraph, BoolMap >& graph,
						const Digraph::NodeMap<unsigned long long>& distances,
						const Digraph::NodeMap<bool>& discovered,
						const Digraph::NodeMap<bitsetN>& common,
						float t_param);
		};

		class DijkstraX : public CMCSingle {
			public:
				DijkstraX() : score_fn(&score_best) { }

				DijkstraX(ScoreFunction custom) :
					CMCSingle(), score_fn(custom) { }

				~DijkstraX() { }

				void solve(const Digraph& graph, const Flow& flow);

				const std::string fullname(void) const {
					return "Dijkstra X";
				}

				const std::string abbrev_name(void) const {
					return "DijkstraX";
				}

			private:
				ScoreFunction score_fn;

				static inline double score(unsigned long long distance,
						unsigned char common_classes) {
					return distance / 50.0 + (80.0 / common_classes);
				}

				static inline double score_best(unsigned long long dist,
						unsigned char common_classes) {
					return dist + common_classes;
				}
		};
	} /* namespace algorithms */
} /* namespace cmc */

#endif /* __DIJKSTRA_H__ */
