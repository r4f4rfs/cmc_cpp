#ifndef __COMMON_H__
#define __COMMON_H__

#include <bitset>
#include <chrono>

#define NCLASSES 8

const unsigned long long TIME_LIMIT = 2 * 60; // 2min

typedef std::bitset<NCLASSES> bitsetN;
typedef std::chrono::microseconds TimeT;

template<typename TimeTT = std::chrono::milliseconds>
struct measure
{
	template<typename F, typename ...Args>
	static typename TimeTT::rep execution(F func, Args&&... args)
	{
		auto start = std::chrono::system_clock::now();
		func(std::forward<Args>(args)...);
		auto duration = std::chrono::duration_cast<TimeTT>(std::chrono::system_clock::now() - start);

		return duration.count();
	}

	template<typename F, typename ...Args>
	static TimeTT duration(F func, Args&&... args)
	{
		auto start = std::chrono::system_clock::now();
		func(std::forward<Args>(args)...);

		return std::chrono::duration_cast<TimeTT>(std::chrono::system_clock::now()-start);
	}
};


#endif /* __COMMON_H__ */
