#include <cstdlib>
#include <iostream>
#include <sstream>
#include <lemon/random.h>
#include <lemon/smart_graph.h>
#include <lemon/lgf_writer.h>

#include "common.h"

static const unsigned int N_CLASSES = 8;

using namespace std;

typedef lemon::IdMap<const lemon::SmartDigraph, lemon::SmartDigraph::Node> IntIdMap;
typedef lemon::SmartDigraph::ArcMap<unsigned int> IntArcMap;
typedef lemon::SmartDigraph::ArcMap<bitsetN> BitArcMap;

static void
gen_random_graph(const string& name, unsigned int nodes, float arc_density,
				float cls_density)
{
	lemon::SmartDigraph graph;

	graph.reserveNode(nodes);
	for (unsigned int i = 0; i < nodes; i++)
		graph.addNode();
	IntIdMap node_map(graph);

	// Worst case is a complete graph
	graph.reserveArc(nodes * nodes * arc_density);

	unsigned int num_arcs = 0;
	for (lemon::SmartDigraph::NodeIt i(graph); i != lemon::INVALID; ++i) {
		for (lemon::SmartDigraph::NodeIt j(graph); j != lemon::INVALID; ++j) {
			if ((i == j) || (lemon::rnd() > arc_density))
				continue;
			graph.addArc(i, j);
			num_arcs++;
		}
	}

	std::cout << "Total arcs / generated arcs: " << (nodes * nodes) << " " << num_arcs << std::endl;

	IntArcMap weight_map(graph);
	BitArcMap class_map(graph);
	for (lemon::SmartDigraph::ArcIt a(graph); a != lemon::INVALID; ++a) {
		weight_map[a] = lemon::rnd[1000] + 10;
		class_map[a] = bitsetN(~0);
	}

	unsigned long long cls_to_remove =
		num_arcs * N_CLASSES * (1 - cls_density);
	unsigned long long cls_remaining = N_CLASSES * num_arcs - cls_to_remove;
	unsigned long long cls_before = cls_to_remove;

	std::cout << "Total classes / classes left: " << (N_CLASSES * num_arcs) << " " << cls_remaining << std::endl;

	std::cout << "Classes to remove: " << cls_to_remove << std::endl;
	lemon::ArcLookUp<lemon::SmartDigraph> lookup(graph);
	while(cls_to_remove) {
		if (cls_before - cls_to_remove > 100000) {
			std::cout << "Classes to remove: " << cls_to_remove << std::endl;
			cls_before = cls_to_remove;
		}
		int i = lemon::rnd[nodes];
		unsigned int ntries = 1000;
		while (ntries--) {
			int j = lemon::rnd[nodes];
			if (j == i)
				continue;
			lemon::SmartDigraph::Arc a = lookup(node_map(i), node_map(j));
			if (a == lemon::INVALID)
				continue;
			if (class_map[a].count() == 1)
				continue;
			class_map[a].set(lemon::rnd[N_CLASSES], 0);
			break;
		}
		if (ntries < 1000)
			cls_to_remove--;
	}

	stringstream title;
	title << nodes << " nodes, " << num_arcs << " arcs and " <<
		cls_remaining << " classes";

	lemon::DigraphWriter<lemon::SmartDigraph>(graph, name).
		nodeMap("nodes", node_map).
		arcMap("weight", weight_map).
		arcMap("classes", class_map).
		attribute("title", title.str()).
		run();
}

static void
usage(void)
{
	std::cerr << "Usage: <num_nodes> <arc_density> <class_density> <name>" << std::endl;
}

int
main(int argc, const char *argv[])
{
	if (argc != 5) {
		usage();
		return -1;
	}

	unsigned int nodes = strtol(argv[1], NULL, 10);
	if (nodes == 0)
		return -1;

	float arc_den = atof(argv[2]);
	if (arc_den < 0 || arc_den > 1.0)
		return -2;

	float cls_den = atof(argv[3]);
	if (cls_den < 0 || cls_den > 1.0)
		return -3;

	gen_random_graph(argv[4], nodes, arc_den, cls_den);

	return 0;
}
