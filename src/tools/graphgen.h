#ifndef __GRAPHGEN__H__
#define __GRAPHGEN__H__


namespace tools {
	static void gen_random_graph(const string& name, unsigned int nodes, float arc_density, float cls_density);
}

#endif /* __GRAPHGEN__H__ */
