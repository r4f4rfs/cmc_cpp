#include <iostream>

#include <lemon/random.h>
#include <lemon/time_measure.h>

#include "simulator.h"

using namespace cmc;
using namespace cmc::concepts;
using namespace cmc::algorithms;

typedef lemon::IdMap<const Digraph, Digraph::Node> IntIdMap;

void
Simulator::setup(const std::string& filename, unsigned char npaths)
{
	lemon::Timer t(false);

	std::cerr << "Loading graph... ";
	t.start();
	//auto start = std::chrono::system_clock::now();
	this->graph.load(filename);
	//auto duration = std::chrono::duration_cast<TimeT>(std::chrono::system_clock::now() - start).count();
	//std::cerr << measure<>::execution(Digraph::load, filename) << std::endl;
	t.halt();
	//std::cerr << duration << std::endl;
	std::cerr << t.realTime() << std::endl;

	this->flow.set_paths(npaths);

	int size = lemon::countNodes(graph);
	IntIdMap map(graph);

	Digraph::Node source = map(lemon::rnd[size]);
	this->flow.set_source(source);

	Digraph::Node target = lemon::INVALID;
	do {
		target = map(lemon::rnd[size]);
	} while (target == source);
	this->flow.set_target(target);

	int nclasses;
	do {
		nclasses = lemon::rnd[4] + 2;
	} while (nclasses < npaths);
	this->flow.set_classes(nclasses);

	std::cerr << "Preprocessing... ";
	t.restart();
	auto nrem = CMC::preprocess(graph, flow);
	//std::cerr << measure<>::execution(CMC::preprocess, graph, flow) << std::endl;
	std::cerr << t.realTime() << std::endl;
	std::cerr << "Arcs removed during preprocessing: " << nrem << std::endl;

	t.halt();
}

void
Simulator::run(CMC& algorithm)
{
	std::cerr << "Running " << algorithm.fullname() << std::endl;
	reset_streams();

	if (!algorithm.is_soluble(this->graph, this->flow)) {
		this->result_msg << algorithm.abbrev_name();
		this->result_msg << " | 1 | [] | 100000 | 100000";
		this->error_msg << "Solution is impossible";
		return;
	}

	lemon::Timer t;
	try {
		//auto start = std::chrono::system_clock::now();
		algorithm.solve(this->graph, this->flow);
		//auto duration = std::chrono::duration_cast<TimeT>(std::chrono::system_clock::now() - start).count();
		//std::cerr << duration << std::endl;
	} catch (CMC::error& error) {
		this->error_msg << error.what();
	}
	t.halt();

	algorithm.print(this->result_msg, this->graph, this->flow);
	if (!algorithm.blocked(this->flow))
		this->result_msg << " | " << t.realTime();
}
