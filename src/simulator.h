#ifndef __SIMULATOR_H__
#define __SIMULATOR_H__

#include <vector>
#include <iostream>
#include <sstream>

#include <lemon/path.h>

#include "concepts/graph.h"
#include "concepts/flow.h"
#include "algorithms/cmc.h"

namespace cmc {
	using namespace concepts;
	using namespace algorithms;

	class Simulator {
		private:
			Flow flow;
			Digraph graph;
			std::ostringstream result_msg;
			std::ostringstream error_msg;

			inline void reset_streams(void) {
				/* Reset string streams */
				this->result_msg.str("");
				this->result_msg.clear();
				this->error_msg.str("");
				this->error_msg.clear();
			}

		public:
			Simulator() { }

			~Simulator() {}

			void setup(const std::string& filename, unsigned char npaths);

			inline std::string flow_str() const {
				return flow.repr(graph);
			}

			inline std::string result_message() const {
				return result_msg.str();
			}

			inline std::string error_message() const {
				return error_msg.str();
			}

			inline int result_code() const {
				return error_msg.str().size();
			}

			void run(CMC& algorithm);

			static const unsigned char SUCCESS = 0;
	};
}

#endif /* __SIMULATOR_H__ */
