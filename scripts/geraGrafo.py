from __future__ import print_function
import math
import sys, os
import random

totalClasses = 8


def geraGrafos(name, nos, denArestas, denClasses):
	#print(str(nos) +"\n" + str(denArestas) + "\n" + str(denClasses) + "\n")

	grafo = [[[0 for _ in range(totalClasses+1)] for _ in range(nos)] for _ in range(nos)]
	numArestas = 0
	posArestas = []
	#imprimeGrafo2(grafo)
	for i in range(nos):
		for j in range(nos):
			if i == j:
				grafo[i][j][0] = 0 # Sem loops
				continue
			if(random.random() <= denArestas):
				numArestas = numArestas +1
			posArestas.append((i,j))
			grafo[i][j][0] = 1 #tem a aresta i,j
			for k in range(1,totalClasses+1):
				grafo[i][j][k] = k #possui todas classes

	#print(posArestas)
	#imprimeGrafo(grafo)

	classesRemover = int(numArestas*totalClasses*(1-denClasses))

	print("Total Arestas/ Arestas Geradas: " +str(nos*nos)+"/"+ str(numArestas))
	remainClasses = totalClasses * numArestas - classesRemover
	print("Total Classes/Classes Deixar: "+str(totalClasses*numArestas)+"/"+str(remainClasses))
	classesAntes = classesRemover
	print("Classes a ser Removidas:" + str(classesRemover))   
	while(classesRemover > 0):
		if(classesAntes - classesRemover > 100000):
			print("Classes a ser Removidas:" + str(classesRemover))
			classesAntes = classesRemover
		pos = random.randint(0,len(posArestas)-1)
		(i,j) = posArestas[pos]
		grafo[i][j].pop(random.randint(1,len(grafo[i][j])-1))
		if(len(grafo[i][j]) == 2): #deixa aresta com pelo menos uma classe
			posArestas.pop(pos)
		classesRemover = classesRemover - 1
	#print("\n\n\n\n\n\n\n\n\n")
	#imprimeGrafo(grafo)
	#salvaGrafo(grafo)
	salvaGrafo2(name, grafo, numArestas, remainClasses)

def salvaGrafo(grafo):
	f = open("grafo.txt", "w")
	nos = len(grafo[0])
	for i in range(nos):
		for j in range(nos):
			if(grafo[i][j][0] == 1):
				f.write("("+str(i)+","+str(j)+") :")
			classes = len(grafo[i][j])
			for k in range(1,classes):
				f.write(str(grafo[i][j][k])+",")
			f.write("\n")
	f.close()

def salvaGrafo2(name, grafo, numArestas, numClasses):
	with open(name, "w") as f:
		nos = len(grafo[0])
		f.write("@nodes\n")
		f.write("label\n")
		for i in range(nos):
			f.write(str(i) + "\n")
		f.write("@arcs\n")
		f.write("\t\tlabel\tweight\tclasses\n")
		lbl = 0
		for i in range(nos):
			for j in range(nos):
				if grafo[i][j][0] == 0:
					continue
				w = random.randint(10, 1000)
				classes = 0
				for k in range(1, len(grafo[i][j])):
					classes = classes | (1 << grafo[i][j][k] - 1)
				f.write("\t".join((str(i), str(j), str(lbl), str(w), str(bin(classes))[2:], "\n")))
				lbl = lbl + 1
		f.write("@attributes\n")
		f.write("title \"%d nodes, %d arcs and %d classes\"\n" %
				(nos, numArestas, numClasses))

def imprimeGrafo(grafo):
	nos = len(grafo[0])
	for i in range(nos):
		for j in range(nos):
			if(grafo[i][j][0] == 1):
				print("("+str(i)+","+str(j)+") :")
			classes = len(grafo[i][j])
			for k in range(1,classes):
				print(str(grafo[i][j][k])+",")
			print("\n")


def imprimeGrafo2(grafo):
	nos = len(grafo[0])
	for i in range(nos):
		for j in range(nos):
			print("("+str(i)+","+str(j)+") :")
			classes = len(grafo[i][j])
		for k in range(0,classes):
			print(str(grafo[i][j][k])+",")
		print("\n")


if __name__ == '__main__':
	if len(sys.argv) != 5:
		print("Usage:", sys.argv[0],
				"<numNos> <denAresta> <denClasses> <graph_name>")
		sys.exit(1)
	try:
		numNos = int(sys.argv[1])
		denArestas = float(sys.argv[2])
		denClasses = float(sys.argv[3])
		name = sys.argv[4]
	except ValueError as e:
		print("Error: ", e.message)
		sys.exit(1)

	if denArestas > 1.0 or denArestas < 0.0 or \
		denClasses > 1.0 or denClasses < 0.0:
		print("Error: densities must be given in the interval [0, 1]")
		sys.exit(1)

	geraGrafos(name, numNos, denArestas, denClasses)
	sys.exit(0)
