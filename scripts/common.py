#!/usr/bin/env python3

from collections import namedtuple

ResultLine = namedtuple('ResultLine', ['blocked', 'value'])

def read_results_from_file(criteria, filename):
	try:
		size = int(filename.split('-')[1])
	except ValueError:
		print('Filename not like rand-<size>-<edges>-<classes>-<exec>')
		raise
	#if size not in SIZES:
	#	raise ValueError('Invalid size %d' % size)

	with open(filename, 'r') as f:
		for n, line in enumerate(f.readlines()):
			try:
				alg, block, _, cost, time = line.split('|')
			except ValueError as e:
				print('Could not parse line', n, 'of', filename)
				raise
			if criteria == 'time':
				yield alg.strip(), size, ResultLine(bool(int(block)), float(time) * 1000.0)
			else:
				yield alg.strip(), size, ResultLine(bool(int(block)), int(cost))

def read_results(criteria, sizes, algorithms, arqlist):
	"""
	Read the results from the list of files and save them in a dictionary
	indexed by Algorithm and then by instance size.
	"""
	res = { alg: { size: [] for size in sizes } for alg in algorithms }
	for arq in arqlist:
		for alg, size, result in read_results_from_file(criteria, arq):
			if alg not in algorithms:
				continue
			res[alg][size].append(result)
	return res
