#!/usr/bin/env python3

import sys
import common
import argparse
from math import sqrt
from itertools import compress
from operator import attrgetter
from collections import namedtuple

CMC_PROBLEM = 'cmc'
MCMC_PROBLEM = 'mcmc'

DEF_SIZES = {
	CMC_PROBLEM: (100, 250, 500, 750, 1000, 2500),
	#CMC_PROBLEM: (10, 25, 50, 75, 100),
	MCMC_PROBLEM: (10, 25, 50, 75, 100)#, 250),
}

ALGORITHMS = {
	#CMC_PROBLEM: ('BranchBnd', 'DijkstraQ', 'DijkstraT', 'DijkstraX', 'Intersect', 'InterFast', ),
	CMC_PROBLEM: ('BranchBnd',),
	#CMC_PROBLEM: ('BranchBnd', 'LPFast', 'LPOrig'),
	#MCMC_PROBLEM: ('MBBound', 'MCMCMin', 'LPMulti')
	MCMC_PROBLEM: ('MCMCMin', 'LPMulti')
}

NEXECS = 30

STATISTIC = namedtuple('Statistic', ['mean', 'min', 'max', 'svar'])

def mean(lst):
	return sum(lst) / len(lst) if lst else 0

def svar(lst):
	m = mean(lst)
	return sqrt(sum((x - m)**2 for x in lst) / (len(lst) - 1)) if len(lst) > 1 else 0

def get_blocks(results, algorithms, size):
	"""
	Return which of the 30 executions for each instance size had at least
	one algorithm that blocked when executing.
	"""
	return tuple(any(x.blocked for x in results[alg][size]) for alg in algorithms)

def get_non_blocks(results, algorithms, size):
	"""
	Return which of the 30 executions did not have any blocked algorithms
	"""
	return tuple(all(not results[alg][size][i].blocked for alg in algorithms) \
				for i in range(NEXECS))

def get_valid_values(results, algorithms):
	"""
	Return only those results where any of the algorithms blocked for that
	particular instance size
	"""
	values = { size: { alg: [] for alg in algorithms } for size in SIZES }
	for size in SIZES:
		not_blocked = get_non_blocks(results, algorithms, size)
		for alg in algorithms:
			values[size][alg] = [x.value for x in compress(results[alg][size], not_blocked)]
	return values

def print_results(results, algorithms, output):
	print('{:<10}'.format('#Algorithm'), '{:>10}'.format('Mean'),
			'{:>8}'.format('Min'), '{:>8}'.format('Max'),
			'{:>8}'.format('Var'))

	for size in SIZES:
		print('#Size {:d}'.format(size))
		for alg in algorithms:
			vmean, vmin, vmax, vsvar = results[size][alg]
			if output == "time":
				print('{:12s}'.format(alg), '{:8.3f}'.format(vmean),
						'{:8.3f}'.format(vmin),
						'{:8.3f}'.format(vmax),
						'{:8.3f}'.format(vsvar), sep=' ')
			else:
				print('{:12s}'.format(alg), '{:8.2f}'.format(vmean),
						'{:8.2f}'.format(vmin),
						'{:8.2f}'.format(vmax),
						'{:8.2f}'.format(vsvar), sep=' ')
		print() # Empty line

def parse_arguments():
	# Usage: %s <cmc|mcmc> <time|cost> [<arqlist>]
	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--problem", choices=[CMC_PROBLEM, MCMC_PROBLEM],
			required=True, help="Problem type")
	parser.add_argument("-c", "--criteria", choices=['time', 'cost'],
			required=True, help="Evaluation criteria to use")
	parser.add_argument("files", metavar='arqlist', nargs='+',
			help='File list to be processed')
	args = parser.parse_args()
	return args.problem, args.criteria, args.files


if __name__ == '__main__':
	global SIZES
	problem, criteria, files = parse_arguments()
	SIZES = DEF_SIZES[problem]
	results = common.read_results(criteria, SIZES, ALGORITHMS[problem], files)
	values = get_valid_values(results, ALGORITHMS[problem])

	stats = {}
	for size in SIZES:
		stats[size] = {}
		for alg in ALGORITHMS[problem]:
			vmean, vsvar = mean(values[size][alg]), svar(values[size][alg])
			vmin, vmax = min(values[size][alg]), max(values[size][alg])
			stats[size][alg] = STATISTIC(vmean, vmin, vmax, vsvar)

	print_results(stats, ALGORITHMS[problem], criteria)

	sys.exit(0)
