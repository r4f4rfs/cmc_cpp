#!/usr/bin/env python3

import sys
import argparse
from math import sqrt
from operator import attrgetter
from collections import namedtuple, defaultdict

ResultLine = namedtuple('ResultLine', ['blocked', 'colours', 'densities', 'order', 'value'])

ALGORITHM="BranchBnd"
DENSITY_PAIRS = [(10, 10), (10, 70), (30, 40), (70, 40), (90, 80)]
SIZES = [100, 250, 500, 750, 1000, 2500]
NEXECS = 30
NPAIRS = 18

def statistics(values):
	if len(values) == 0:
		return 0
	m = sum(values) / len(values)
	svar = sqrt(sum((x - m) ** 2 for x in values) / (len(values) - 1))
	ci = 1.96 * svar / sqrt(len(values))
	return m, svar, ci

def get_values(results):
	return list(map(attrgetter('value'), results))

def get_means_by_k(results):
	print("# Results by k")
	for i in range(2, 6):
		print("# k =", i, "colours")
		print("#size", "Mean", "Svar", "CI", sep="\t")
		filtered = { size: [x for x in results[size] if x.colours is i] for size in SIZES }
		for size in SIZES:
			m, svar, ci = statistics(get_values(filtered[size]))
			print(size, "{:3.2f}".format(m), "{:3.2f}".format(svar),
					"{:3.2f}".format(ci), sep="\t")
	print()

def get_means_by_densities(results):
	print("# Results by density")
	for dp, dc in DENSITY_PAIRS:
		print("# Density ({:2.1f}".format(dp / 100.), ",{:2.1f})".format(dc / 100.), sep="")
		print("#size", "Mean", "Svar", "CI", sep="\t")
		filtered = { size: [x for x in results[size] if x.densities == (dp, dc) ] \
				for size in SIZES }
		for size in SIZES:
			m, svar, ci = statistics(get_values(filtered[size]))
			print(size, "{:3.2f}".format(m), "{:3.2f}".format(svar),
					"{:3.2f}".format(ci), sep="\t")
	print()

def remove_blockeds(results):
	new_res = defaultdict(list)
	for i in range(NEXECS * NPAIRS):
		if all(not results[size][i].blocked for size in SIZES):
			for size in SIZES:
				new_res[size].append(results[size][i])
	return new_res

def read_results_from_file(criteria, algorithm, filename):
	name_split = filename.split('-')
	try:
		size = int(name_split[1])
		dp = int(float(name_split[2]) * 100)
		dc = int(float(name_split[3]) * 100)
		order = int(name_split[4].split('.')[0])
	except ValueError:
		print('Filename not like rand-<size>-<edges>-<classes>-<exec>')
		raise

	with open(filename, 'r') as f:
		errfile = filename[:-3] + "err"

		with open(errfile, 'r') as ef:
			colours = int(ef.readlines()[4].split(" ")[3])

		for n, line in enumerate(f.readlines()):
			try:
				alg, block, _, cost, time = line.split('|')
			except ValueError as e:
				print('Could not parse line', n, 'of', filename)
				raise
			if alg.strip() != algorithm:
				continue
			if criteria == 'time':
				yield size, ResultLine(bool(int(block)), colours, (dp, dc), order, float(time) * 1000.0)
			else:
				yield size, ResultLine(bool(int(block)), colours, (dp, dc), order, int(cost))

def read_results(criteria, sizes, algorithm, arqlist):
	"""
	Read the results from the list of files and save them in a dictionary
	indexed by Algorithm and then by instance size.
	"""
	res = defaultdict(list)
	for arq in arqlist:
		for size, result in read_results_from_file(criteria, algorithm, arq):
			res[size].append(result)
	return res

def parse_arguments():
	""" Usage: %s <cmc|mcmc> <time|cost> [<arqlist>] """
	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--problem", choices=['cmc','mcmc'],
			required=True, help="Problem type")
	parser.add_argument("-c", "--criteria", choices=['time', 'cost'],
			required=True, help="Evaluation criteria to use")
	parser.add_argument("files", metavar="arqlist", nargs='+',
			help="File list to be processed")
	args = parser.parse_args()
	return args.problem, args.criteria, args.files

if __name__ == '__main__':
	problem, criteria, files = parse_arguments()
	results = read_results(criteria, SIZES, ALGORITHM, files)
	non_blocked = remove_blockeds(results)
	get_means_by_densities(non_blocked)
	get_means_by_k(non_blocked)


