#!/bin/sh

DATA_DIR="$HOME/prjs/cmc3/results/rand/multi_path/disjoint/"
#DATA_DIR="$HOME/prjs/cmc3/results/rand/single_path/heuristic_results/"
SRC_DIR="$HOME/prjs/cmc3/src"
PLOT_TIME_SCRIPT="${SRC_DIR}/scripts/gplot_mtimes.p"
#PLOT_TIME_SCRIPT="${SRC_DIR}/scripts/gplot_times.p"

plot_cost_profile() {
	for i in $(find ${DATA_DIR} -maxdepth 1 -name "cmc_cost_*.dat"); do
		name=$(basename $i)
		echo "Plotting cost data file ${name}"
		sed -i "s/plot .*/plot '${name}' \\\\/" ${PLOT_COST_SCRIPT}
		output="probs$(echo ${name} | sed -E 's/(cmc)|(dat)//g')eps"
		gnuplot -e "output_file='${output}'" ${PLOT_COST_SCRIPT}
	done
}

plot_time_profile() {
	for i in $(find ${DATA_DIR} -maxdepth 1 -name "times_*.dat"); do
		name=$(basename $i)
		echo "Plotting time data file ${name}"
		sed -i "s/plot .*/plot '${name}' \\\\/" ${PLOT_TIME_SCRIPT}
		output="$(echo ${name} | sed -E 's/(cmc)|(dat)//g')eps"
		gnuplot -e "output_file='${output}'" ${PLOT_TIME_SCRIPT}
	done
}

#plot_cost_profile
plot_time_profile
