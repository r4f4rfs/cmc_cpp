#!/usr/bin/env python2
from __future__ import print_function
from __future__ import division

import sys
from collections import defaultdict
from math import sqrt

def mean(lst):
	return sum(lst) / len(lst) if lst else 0

def stats(lst):
	m = mean(lst)
	svar = sqrt(sum((x - m)**2 for x in lst) / (len(lst) - 1)) if len(lst) > 1 else 0
	return m,svar,1.96 * svar / sqrt(len(lst))

def print_results(results):
	print('{:<6}'.format("#Load"), '{:<5}'.format('Mean'), '{:<5}'.format("Svar"), '{:<5}'.format("CI"))
	for load,values in sorted(results.items()):
		mean, svar,  conf_inter = values
		print('{:4d}'.format(load), ' {:5.2f}'.format(mean), ' {:5.3f}'.format(svar), ' {:5.3f}'.format(conf_inter))

if __name__ == '__main__':
	if len(sys.argv) == 0:
		print("Usage:", sys.argv[0], "<filename>")
		sys.exit(1)
	values = defaultdict(list)
	with open(sys.argv[1], 'r') as f:
		output = f.readlines()
		for i in range(2, len(output), 3):
			load = int(output[i - 2][len("Load:"):-3])
			values[load].append(float(output[i].split(' ')[4]))
	results = { load : stats(lst) for load, lst in values.items() }
	print_results(results)
