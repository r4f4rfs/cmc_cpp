#!/usr/bin/env python3

import sys
import common
import argparse
from math import ceil
from itertools import chain
from operator import attrgetter
from collections import defaultdict

CMC_PROBLEM = 'cmc'
MCMC_PROBLEM = 'mcmc'

DEF_SIZES = {
	CMC_PROBLEM: (100, 250, 500, 750, 1000, 2500),
	#MCMC_PROBLEM: (10, 25, 50, 75, 100, 250),
	MCMC_PROBLEM: (100, 250, 500, 750, 1000, 2500),
}

ALGORITHMS = {
	CMC_PROBLEM: ('BranchBnd', 'DijkstraQ', 'DijkstraT', 'DijkstraX', 'Intersect', 'InterFast', 'LPFast', 'LPOrig'),
	MCMC_PROBLEM: ('MBBound', 'MCMCMin', 'LPMulti')
	#MCMC_PROBLEM: ('MCMCMin', 'LPMulti')
}

NEXECS = 30 * 6 * 3
MAX_RATIO = 10000

def get_max_ratio(ratios, algorithms):
	"""
	Get the ratio between the best and worst performing algorithms
	"""
	all_results = chain(*(ratios[s][a] for a in algorithms for s in SIZES))
	return max(filter(lambda val: val < MAX_RATIO, all_results))

def calculate_probabilities(results, algorithms):
	"""
	Calculate the probabilities of each algorithm having a result <= Tau * OPT,
	where OPT is the best result found for that instance size
	"""
	ratios = calculate_ratios(results, algorithms)
	max_ratio = ceil(get_max_ratio(ratios, algorithms))
	total = NEXECS * len(SIZES) # executions for each algorithm
	all_sizes = lambda alg: chain(*(ratios[size][alg] for size in SIZES))
	lt_tau = lambda tau, alg: sum(x <= tau for x in all_sizes(alg))
	max_ttau = 2 * max_ratio + 1
	res = { alg: tuple(lt_tau(ttau / 2, alg) / total \
			for ttau in range(2, max_ttau)) for alg in algorithms }
	return res, max_ratio

def calculate_ratios(results, algorithms):
	"""
	Calculate the ratios of algorithm's result for the best execution of all
	algorithms for that same instance.
	"""
	exts = find_extremes(results, algorithms)
	div_nblock = lambda t: t[0].value / t[1] if not t[0].blocked else MAX_RATIO
	return { size: { alg:
		tuple(map(div_nblock, zip(results[alg][size], exts[size]))) \
		for alg in algorithms } for size in SIZES }

def find_extremes(results, algorithms):
	"""
	Return the best result for the set of NEXECS of a particular size
	instance. Ignore blocked algorithms
	"""
	values = lambda lst: map(attrgetter('value'), lst)
	not_blocked = lambda lst: filter(lambda x: not x.blocked, lst)
	return { size: tuple(min(values(not_blocked(results[alg][size][i] \
		for alg in algorithms)), default=MAX_RATIO) \
		for i in range(NEXECS)) \
		for size in SIZES }

def print_probabilities(results, algorithms, mratio, threshold):
	print('# Ratio\t', *algorithms)
	last = {alg: 0 for alg in algorithms}
	no_changes_count = 0
	step = 1 if mratio <= 20 else ceil(mratio / 20)
	#for idx, ttau in enumerate(range(2, 2 * mratio + 1, step)):
		#print("{:03.1f}".format(ttau / 2).ljust(9), end='')
	#for ttau in chain(*([1], range(step, mratio + step + 1, step))):
	for ttau in [1, 2, 5, 10, 15, 20, 25, 50, 75, 100, 250, 500, 1000, 5000, 10000]:
		print("{:03.1f}".format(ttau).ljust(9), end='')
		changed = False
		for alg in algorithms:
			val = 0.0
			try:
				#val = results[alg][idx]
				val = results[alg][ttau]
			except IndexError:
				val = results[alg][-1]
			print("{:.3f}".format(float(val)).ljust(len(alg)), end=' ')
			if val != last[alg]:
				changed = True
			last[alg] = val
		print()
		# There is not change in results
		no_changes_count = no_changes_count + 1 if not changed else 0
		# After 'threshold' consecutive no changes, stop printing results
		if threshold > 0 and no_changes_count >= threshold:
			return

def parse_arguments():
	# Usage: %s <cmc|mcmc> <time|cost> [<arqlist>]
	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--problem", choices=[CMC_PROBLEM, MCMC_PROBLEM],
			required=True, help="Problem type")
	parser.add_argument("-c", "--criteria", choices=['time', 'cost'],
			required=True, help="Evaluation criteria to use")
	parser.add_argument("-t", "--threshold", type=int, default=0,
			help="""Number of repetitions before stop processing the results.
					0 means non-stop""")
	parser.add_argument("files", metavar='arqlist', nargs='+',
			help='File list to be processed')
	args = parser.parse_args()
	return args.problem, args.criteria, args.threshold, args.files

if __name__ == '__main__':
	global SIZES
	problem, criteria, threshold, files = parse_arguments()
	SIZES = DEF_SIZES[problem]
	results = common.read_results(criteria, SIZES, ALGORITHMS[problem], files)
	probs, mratio = calculate_probabilities(results, ALGORITHMS[problem])
	print_probabilities(probs, ALGORITHMS[problem], mratio, threshold)

