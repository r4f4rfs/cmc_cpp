#include <cstdio>
#include <iostream>

#include <lemon/random.h>
#include <lemon/time_measure.h>

#include "algorithms/cmc.h"
#include "algorithms/dijkstra.h"
#include "simulator.h"

using namespace std;
using namespace cmc;
using namespace algorithms;

ScoreFunction score_types[] = {
	[](unsigned long long d, unsigned char c) { return d/50.0 + 80.0/c; },
	[](unsigned long long d, unsigned char c) { return d/30.0 + 50.0/c; },
	[](unsigned long long d, unsigned char c) { return d/10.0 + c; },
	[](unsigned long long d, unsigned char c) { return d/50.0 + 50.0 * c; },
	[](unsigned long long d, unsigned char c) { return d/10.0 + 5.0 * c; },
	[](unsigned long long d, unsigned char c) { return d + 50.0 * c; },
	[](unsigned long long d, unsigned char c) { return (double)d / c; },
	[](unsigned long long d, unsigned char c) { return (double)d + c; }, /* best */
	[](unsigned long long d, unsigned char c) { return (double)d + c * c; },
	[](unsigned long long d, unsigned char c) { return sqrt(d) + c; },
	[](unsigned long long d, unsigned char c) { return 1.0/d + c; },
};

static inline void
print_usage(char *name)
{
	cerr << "Usage: " << name << " <seed> <graph.lgf>" << endl;
}

int
main(int argc, char *argv[])
{
	lemon::Timer t;
	unsigned int seed;

	if (argc < 3) {
		print_usage(argv[0]);
		exit(EXIT_FAILURE);
	}

	seed = strtol(argv[1], NULL, 10);

	lemon::rnd.seed(seed);

	Simulator sim;
	sim.setup(argv[2], 1);

	cerr << sim.flow_str() << endl;

	unsigned int ntypes = sizeof(score_types) / sizeof(score_types[0]);
	for (unsigned int i = 0; i < ntypes; ++i) {
		CMC *algorithm = new DijkstraX(score_types[i]);
		sim.run(*algorithm);
		cout << sim.result_message() << endl;
		if (sim.result_code() != Simulator::SUCCESS) {
			cerr << sim.error_message() << endl;
		}
		delete algorithm;
	}

	t.halt();
	cerr << "Simulation runtime: " << t.realTime() <<  endl;

	return 0;
}
