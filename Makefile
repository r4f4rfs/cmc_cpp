CC = g++ -std=c++0x

HAVE_CPLEX = 1

ENABLE_DEBUG := 0

CPLEX = /opt/CPLEX/opl
SRC_DIR = ./src
BIN_DIR = ./bin
TST_DIR = ./tests
ALGS_DIR = $(SRC_DIR)/algorithms
TOOLS_DIR = $(SRC_DIR)/tools

CFLAGS = -DIL_STD -DILOUSEMT -D_REENTRANT -I$(HOME)/dev/include -I$(SRC_DIR)

ifeq ($(ENABLE_DEBUG), 1)
	CFLAGS += -g -O0 -Wall -Wextra -pg
else
	CFLAGS += -O2
endif

ifeq ($(HAVE_CPLEX), 1)
	CFLAGS += -I$(CPLEX)/include -DHAVE_CPLEX=1
endif

LDFLAGS = -L$(HOME)/dev/lib -lemon -pthread

ifeq ($(HAVE_CPLEX), 1)
	LDFLAGS += -L$(CPLEX)/lib/x86-64_linux/static_pic \
		   -L$(CPLEX)/bin/x86-64_linux/ -lopl -liljs -lilocplex \
		   -lcp -lconcert -lcplex1260 -ldbkernel -ldblnkdyn -lilog \
		   -ldl -lm
endif

ALGORITHMS = $(ALGS_DIR)/cmc.cpp $(ALGS_DIR)/dummy.cpp \
	     $(ALGS_DIR)/dijkstra.cpp $(ALGS_DIR)/intersection.cpp \
	     $(ALGS_DIR)/backtrack.cpp $(ALGS_DIR)/mcmcmin.cpp

ifeq ($(HAVE_CPLEX), 1)
	ALGORITHMS += $(ALGS_DIR)/ilp.cpp
endif

SOURCES = $(SRC_DIR)/simulator.cpp $(SRC_DIR)/main.cpp

TESTS = $(TST_DIR)/dijkstraX-test.cpp

OBJS = $(patsubst $(SRC_DIR)/%.cpp,$(BIN_DIR)/%.o,$(SOURCES))
OBJS += $(patsubst $(ALGS_DIR)/%.cpp,$(BIN_DIR)/%.o,$(ALGORITHMS))
#TEST_OBJS = $(patsubst $(TST_DIR)/%.cpp,$(BIN_DIR)/%.o,$(TESTS))

DEPS := $(OBJS:.o=.d)

%.o : CFLAGS += -c -MMD -MP

all: cmc graphgen

$(OBJS): | $(BIN_DIR)

$(BIN_DIR):
	@mkdir -p $@

$(BIN_DIR)/%.o: $(ALGS_DIR)/%.cpp
	@echo "Compiling algorithm $@"
	$(CC) $(CFLAGS) $< -o $@

$(BIN_DIR)/%.o: $(SRC_DIR)/%.cpp
	@echo "Compiling $@"
	$(CC) $(CFLAGS) $< -o $@

$(BIN_DIR)/%.o: $(TST_DIR)/%.cpp
	@echo "Compiling test $@"
	$(CC) $(CFLAGS) $< -o $@

cmc: $(OBJS)
	$(CC) $(CFLAGS) $^ -o $(BIN_DIR)/$@ $(LDFLAGS)

DIJX_TEST_OBJS = $(BIN_DIR)/cmc.o $(BIN_DIR)/dijkstra.o $(BIN_DIR)/simulator.o

dijx-test: $(DIJX_TEST_OBJS) $(BIN_DIR)/dijkstraX-test.o
	@echo "Compiling test $@"
	$(CC) $(CFLAGS) $^ -o $(BIN_DIR)/$@ $(LDFLAGS)

graphgen: $(TOOLS_DIR)/graphgen.cpp
	@echo "Compiling tool $@"
	$(CC) $(CFLAGS) $^ -o $(BIN_DIR)/$@ $(LDFLAGS)

clean:
	@rm -rf $(BIN_DIR) *.dSYM

.PHONY: clean

-include $(DEPS)
